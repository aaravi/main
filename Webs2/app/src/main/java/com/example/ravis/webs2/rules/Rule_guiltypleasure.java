package com.example.ravis.webs2.rules;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.goals.Goal_details;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_MERCHANT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;

public class Rule_guiltypleasure extends AppCompatActivity
{

    CardView selectMerchent, linkedbank, saveamount, fundedaccount, goalselection;

    ImageView onback,ondelete;

    final String[] bankvalue = {"Bank 1", "Bank 2", "Bank 3", "Bank 4", "Bank 5"};

    final String[] goalvalue = {"Goal 1", "Goal 2", "Goal 3", "Goal 4", "Goal 5"};

    boolean[] selected = {false, true, false, false, true};

    goalSelection objectForTowards;

    int[] image = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5};

    int set = 2, limit = 500, preset = 30, count = 0;

    AlertDialogs alert;

    Button createguiltyrulebutton;

    TextView gpselectgoals, gptitle,gpatmerchent, goalcount,gpfundedbank;

    private int fundedbankId, field_linkedBank;

    private Boolean create = false;

    private int filed_triggering_bank, ruleid = 2, check = 0;

    TextView gplinkedbank, gpcurrentsave,gpHeading;

    static Context context;

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    private boolean isEdited=false,isclicked=false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_b_guiltypleasure);
        context = this;
        ruleid = this.getIntent().getIntExtra("ruleid", 2);
        create = this.getIntent().getBooleanExtra("type", true);
        bankval = new ArrayList<>();
        bankval = new SqlTransactions(context).fundingBankAccount();
        goalsdata = new ArrayList<>();
        goalsdata = new SqlTransactions(context).getallgoalsforrule(5);
        alert = new AlertDialogs(context);
        objectForTowards = new goalSelection(context);

        onback = (ImageView) findViewById(R.id.gpback);
        ondelete = (ImageView) findViewById(R.id.gpdelete);

        selectMerchent = (CardView) findViewById(R.id.gpmerchant);
        goalselection = (CardView) findViewById(R.id.gptowardgoal);
        linkedbank = (CardView) findViewById(R.id.gpusingbank);
        saveamount = (CardView) findViewById(R.id.gpsave);
        fundedaccount = (CardView) findViewById(R.id.gpfundedbank);

        gpatmerchent = (TextView) findViewById(R.id.gpmerchanttext);
        gplinkedbank = (TextView) findViewById(R.id.gpusingbanktext);
        gpcurrentsave = (TextView) findViewById(R.id.gpsavetext);
        gpfundedbank = (TextView) findViewById(R.id.gpfundedbanktext);
        goalcount = (TextView) findViewById(R.id.gptowardgoaltext);
        gptitle = (TextView) findViewById(R.id.gptitle);
        gpHeading=(TextView) findViewById(R.id.gpDescription);

        createguiltyrulebutton = (Button) findViewById(R.id.gpbutton);

        onback.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {onBackPressed();
            }
        });
        gpatmerchent.addTextChangedListener(new helperfunctions().PageHeadingChanger(gpcurrentsave,gpatmerchent,null,gpHeading,"guilty"));
        gpcurrentsave.addTextChangedListener(new helperfunctions().PageHeadingChanger(gpcurrentsave,gpatmerchent,null,gpHeading,"guilty"));

        init();



        if(create)
        {
            gptitle.setText("Create Guilty Pleasure Rule");
            createguiltyrulebutton.setText("Create Rule");
            ondelete.setVisibility(View.INVISIBLE);
            createguiltyrulebutton.setOnClickListener(creategulitypleasurelistener);
            goalcount.setText(goalsdata==null?"Select Goals":(goalsdata.get(0).getGoal_name()));


            goalselection.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    isEdited=true;
                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });

        } else
        {
            loadAllValues();
            gptitle.setText("Edit Rule");
            createguiltyrulebutton.setOnClickListener(updateRule);
            createguiltyrulebutton.setText("Save");
            goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);
            ondelete.setOnClickListener(deleteRule);
            ondelete.setVisibility(View.VISIBLE);

            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);

            goalselection.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    isEdited=true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);



                }
            });
        }
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder dialog=new AlertDialog.Builder(context,R.style.MyDialogTheme);
        dialog.setTitle("Discard Changes?");
        if(isEdited)
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {

                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    // rounduprule.this.onBackPressed();//long result=new SqlTransactions(context).deleteRule(ruleid)
                    finish();
                }
            }).show();
        else
            finish();
    }

    void init()
    {


        selectMerchent.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
                Intent i = new Intent(context, Rule_SelectMerchant.class);
                startActivity(i);
            }
        });

        linkedbank.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
               /* alert.SingleSelectWithImage("Select Funded Bank",bankvalue,image,new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String x=bankvalue[which];
                        gplinkedbank.setText(x);
                    }
                });*/
                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        String x = bankval.get(which).getBankName();
                        filed_triggering_bank = bankval.get(which).getYodlee_id();
                        gplinkedbank.setText(x);
                    }
                });
            }
        });

        gpcurrentsave.setText("₹" + String.valueOf(preset));
        saveamount.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
                alert.editTextField("SAVE", "Max limit = ₹" + String.valueOf(limit), preset, limit, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        int x = alert.getVal();
                        gpcurrentsave.setText("₹" + String.valueOf(x));
                    }
                });
            }
        });



        fundedaccount.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;

                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        String x = bankval.get(which).getBankName();
                        gpfundedbank.setText(x);
                        field_linkedBank = bankval.get(which).getYodlee_id();
                    }
                });
            }
        });
    }

    private ContentValues getValuesFromFields()
    {

        ContentValues dbvalues = new ContentValues();
        dbvalues.put(RULE_NAME, "Guilty Pleasure Rule");
        dbvalues.put(RULE_IMAGE, R.drawable.img2);
        dbvalues.put(RULE_TRIGERRING_BANK, filed_triggering_bank);
        dbvalues.put(RULE_MERCHANT, gpatmerchent.getText().toString());
        dbvalues.put(RULE_FUNDED_BANK, field_linkedBank);
        String stringValfromSaveValue = gpcurrentsave.getText().toString().replaceAll("[₹]", "").replace("\"", "");
        int selected_amount = Integer.parseInt(stringValfromSaveValue.replace("\"", ""));
        dbvalues.put(RULE_SAVE_AMOUNT, selected_amount);
        return dbvalues;
    }

    private Button.OnClickListener creategulitypleasurelistener = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            objectForTowards.getGoalid(isclicked,goalsdata);

            long result = new SqlTransactions(context).insertRule(getValuesFromFields());
            Toast.makeText(context,""+result,Toast.LENGTH_SHORT).show();

            Toast.makeText(context, result != -1 ? "New Rule created" : "Error creating rule", Toast.LENGTH_SHORT).show();

            new SqlTransactions(context).insertmappingcreateRule(objectForTowards.goalid);

            Toast.makeText(context, result != -1 ? "Mappings Updated" : "Error updataing mappings", Toast.LENGTH_SHORT).show();

            Intent in = new Intent(Rule_guiltypleasure.this, helperfunctions.AfterNewRuleReurnToClass);

            startActivity(in);
        }
    };

    private void loadAllValues()
    {
        // createrounduprulebutton.setText("Save");
        //  alert.finalselected_goals=new SqlTransactions(context).getallgoalsforrule(ruleid);


        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();//to move to fields

        gpcurrentsave.setText(c.getString(2).equals("") ? "0" : "₹" + c.getString(2));//save amount

        field_linkedBank = c.getString(15) == null ? 0 : Integer.parseInt(c.getString(15));//id for funded bank

        gpfundedbank.setText(c.getString(14) == null ? "Please Link Your Bank" : c.getString(14));//funded bank name

        gplinkedbank.setText(c.getString(13) == null ? "Please Link Your Bank" : c.getString(13));//triggering bank name

        filed_triggering_bank = c.getString(12) == null ? 0 : Integer.parseInt(c.getString(12));// id for triggering bank

        gpatmerchent.setText(c.getString(5)); //merchant
    }

    private View.OnClickListener createRule = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {


        }
    };

    private View.OnClickListener updateRule = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            ArrayList<Integer> finalSelected_goals = new ArrayList<>();
            if (objectForTowards.alert.finalselected_goals == null)
            {
                for (int i = 0; i < goalsdata.size(); i++)
                {
                    if (goalsdata.get(i).getRules_id() == ruleid)
                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }
            } else
            {
                finalSelected_goals = objectForTowards.alert.finalselected_goals;
            }
            new SqlTransactions(context).updateRuleWithMappings(getValuesFromFields(), ruleid, finalSelected_goals);
            finish();
        }
    };


    View.OnClickListener deleteRule = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
            dialog.setTitle("Confirm Delete");
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {

                    long result = new SqlTransactions(context).deleteRule(ruleid);
                    Toast.makeText(context, result != -1 ? "Rule Deleted" : "Error deleting rule", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).show();

        }
    };
}