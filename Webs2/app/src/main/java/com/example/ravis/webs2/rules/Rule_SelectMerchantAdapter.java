package com.example.ravis.webs2.rules;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ravis.webs2.R;

/**
 * Created by Kshitij Mittal on 13-06-2017.
 */

public class Rule_SelectMerchantAdapter extends RecyclerView.Adapter<Rule_SelectMerchantAdapter.MyViewHolder> {

    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView bankname;

        public ImageView icon;

        public LinearLayout mcontainer;


        public MyViewHolder(View view) {

            super(view);

            bankname = (TextView) view.findViewById(R.id.bankname);

            icon = (ImageView) view.findViewById(R.id.bankicon);

            mcontainer = (LinearLayout) view.findViewById(R.id.container);

            mcontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }
            });

        }
    }


    public Rule_SelectMerchantAdapter(Context context) {

        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.rule_z_selectmerchantcard, parent, false);

        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.bankname.setText("Merchant " + String.valueOf(position));

        holder.icon.setImageResource(R.drawable.goalicon);


    }

    @Override
    public int getItemCount() {

        return 5;

    }

}
