package com.example.ravis.webs2.rules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.ravis.webs2.R;

public class Rule_SelectMerchant extends AppCompatActivity {
    ImageView backbutton;
    private RecyclerView recyclerView;
    private Rule_SelectMerchantAdapter mAdapter;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_z_selectmerchant);
        context=this;

        backbutton=(ImageView)findViewById(R.id.back);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.rvmerchant);

        mAdapter = new Rule_SelectMerchantAdapter(context);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(mAdapter);
    }
}
