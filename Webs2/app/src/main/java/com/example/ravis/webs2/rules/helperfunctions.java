package com.example.ravis.webs2.rules;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ravis on 6/20/2017.
 * <p>
 * Performs some useful functions for different classes
 */

public class helperfunctions
{

   public static int helperGoalPosition=1;

    public static Class AfterNewRuleReurnToClass;

    //user can enter max 4 digits------------------------------------------------------------------------------------------------
    void inputlimiter(EditText v, int upto)
    {

        InputFilter[] FilterArray = new InputFilter[1];

        FilterArray[0] = new InputFilter.LengthFilter(upto);

        v.setFilters(FilterArray);
    }


    //limits to a value of variable 'limit'-----------------------------------------------------------------------------------------
    //selection sets the cursor to a position after validating the value in edittext
    void limittovalue(final int limit, final int selection, final EditText dialogedittext, final Button okbutton)
    {

        dialogedittext.addTextChangedListener(new TextWatcher()
        {

            int savedval, curval;

            String beforestring, afterstring;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

                beforestring = dialogedittext.getText().toString();

                if (!((beforestring.equals(""))))
                {

                    savedval = Integer.parseInt(beforestring);

                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {

                afterstring = dialogedittext.getText().toString();

                if (!afterstring.equals(""))

                {

                    okbutton.setTextColor(Color.parseColor("#1FA8CE"));

                    okbutton.setEnabled(true);

                    curval = Integer.parseInt(afterstring);

                    if (curval == 0)

                        dialogedittext.setText("");

                    if (curval > limit)

                    {

                        dialogedittext.setText(Integer.toString(savedval));

                        dialogedittext.setSelection(selection);

                        okbutton.setEnabled(true);
                    }
                } else
                {

                    okbutton.setEnabled(false);

                    okbutton.setTextColor(Color.parseColor("#b8d7e0"));
                }
            }
        });

    }


    //changes value of one textview when other changes and returns an instance of textwatcher-------------------------------------------------
    public TextWatcher setHeadingAccordingToView(final TextView source, final TextView destination, final String begin, final String end)
    {

        TextWatcher textWatcher = new TextWatcher()
        {

            String before = "";

            String after = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

                before = source.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {

                after = source.getText().toString();
                if (!(before.equals(after)))
                {
                    destination.setText(begin + " " + source.getText().toString() + " " + end);
                }
            }
        };
        return textWatcher;
    }

    //Takes input as textviews src1 src2,src3( depending upon the textviews changinging)on which textwatcher will be applied
    // so that the values in "heading" change accordingly
    //casee is a string that identify the rule it works for
    public TextWatcher PageHeadingChanger(final TextView src1, final TextView src2, final TextView src3, final TextView heading, final String casee)
    {

        TextWatcher freelancer = null;
        freelancer = new TextWatcher()
        {

            String before_Src1 = "";

            String before_Src2 = "";

            String before_Src3 = "";

            String after_Src1 = "";

            String after_Src2 = "";

            String after_Src3 = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

                before_Src1 = (src1 == null ? "" : src1.getText().toString());
                before_Src2 = (src2 == null ? "" : src2.getText().toString());
                before_Src3 = (src3 == null ? "" : src3.getText().toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {

                after_Src1 = (src1 == null ? "" : src1.getText().toString());
                after_Src2 = (src2 == null ? "" : src2.getText().toString());
                after_Src3 = (src3 == null ? "" : src3.getText().toString());

                switch (casee)
                {
                    case "roundup":
                        if (!before_Src1.equals(after_Src1))
                            heading.setText("Round up every purchase to nearest " + after_Src1 + " and save the difference");
                        break;
                    case "guilty":
                        if (!(before_Src1.equals(after_Src1) && before_Src2.equals(after_Src2)))
                            heading.setText("Save " + after_Src1 + " every time you make a purchase at " + after_Src2);
                        break;
                    case "setforget":
                        if (!(before_Src1.equals(after_Src1) && before_Src2.equals(after_Src2)))
                            heading.setText("Save " + after_Src1 + " every " + after_Src2);
                        break;
                    case "freelancer":
                        if (!(before_Src1.equals(after_Src1) && before_Src2.equals(after_Src2)))
                            heading.setText("Save " + after_Src1 + " on every new deposit that is larger than " + after_Src2);
                        break;
                    case "spendless":
                        if (!(before_Src1.equals(after_Src1) && before_Src2.equals(after_Src2) && before_Src3.equals(after_Src3)))
                            heading.setText("Spend less than " + after_Src2 + " at " + after_Src3 + " during a " + after_Src1 + " and save the difference");
                        break;
                    case "f2week":
                        if (!(before_Src1.equals(after_Src1) && before_Src2.equals(after_Src2)))
                            heading.setText("Save " + after_Src1 + " on every new deposit that is larger than " + after_Src2);
                        break;
                }

            }
        };

        return freelancer;
    }

}

