package com.example.ravis.webs2.rules;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

/**
 * Created by aasthu on 12-Jul-17.
 */

public class helperClass_for_WeekOfCreate52Week_rule {

    String before = "";

    String after = "";

    String aftervalue_reversed = "";

    String aftervalue_regular = "";

    public TextWatcher set_value_in_array(final TextView amount_tobe_deducted) {

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                before = amount_tobe_deducted.getText().toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                after = amount_tobe_deducted.getText().toString();
                if (after != before) {
                    String weekval = after.toString().replaceAll("[A-Za-z]|[$]|[\\s]|[()]|[₹]", "");
                    int amountt = Integer.parseInt(weekval);
                    aftervalue_regular = "regular(Start with ₹" + amountt + ")";
                    aftervalue_reversed = "reversed (start with ₹" + ((52 - amountt) + 1) + ")";
                }


            }
        };
        return textWatcher;
    }
}
