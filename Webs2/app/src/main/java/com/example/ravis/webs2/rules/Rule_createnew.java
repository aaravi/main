package com.example.ravis.webs2.rules;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.ravis.webs2.R;


public class Rule_createnew extends AppCompatActivity {
    CardView myCardView;
    Toolbar t;
    RecyclerView myRecyclerView;
    RecyclerView.LayoutManager myLayoutManager;
    RecyclerView.Adapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_a_createnew);
        t = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(t);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        myCardView = (CardView) findViewById(R.id.maincardview);
        myRecyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);
        myRecyclerView.setNestedScrollingEnabled(false);
        myLayoutManager = new LinearLayoutManager(this);
        myAdapter = new Rule_createNewAdapter(this);
        myRecyclerView.setLayoutManager(myLayoutManager);
        myRecyclerView.setAdapter(myAdapter);

    }
}
