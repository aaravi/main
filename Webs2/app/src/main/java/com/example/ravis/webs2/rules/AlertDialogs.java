package com.example.ravis.webs2.rules;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;


public class AlertDialogs {

    private Context context;

    private int val;

    private int amount;

    int selectedbankid;

    private int check = 0;

    private boolean isselected;

   public ArrayList<Integer> finalselected_goals;

    public AlertDialogs(Context context) {

        this.context = context;

    }


    //FOR EDITEXT
    //FOR EDITEXT
    public void editTextField(String head, String subhead, final int preset, final int limit, DialogInterface.OnClickListener pressok) {

        val = preset;


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        dialogBuilder.setMessage(subhead);
        LinearLayout temp = new LinearLayout(context);
        temp.setOrientation(LinearLayout.HORIZONTAL);
        temp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        final EditText response = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(60, 0, 60, 0);
        response.setLayoutParams(lp);
        response.setInputType(InputType.TYPE_CLASS_NUMBER);
        response.setHint("₹ " + String.valueOf(val));
        temp.addView(response);

        dialogBuilder.setView(temp);


        response.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

                if (source.toString().equals("")) {
                    return null;
                } else if (dest.toString().equals("")) {
                    if (source.equals("0"))
                        return "";
                    else
                        val = Integer.valueOf(source.toString());
                    return null;
                } else {
                    int z = Integer.valueOf(dest.toString()) * 10 + Integer.valueOf(source.toString());
                    if (z > limit) {
                        return "";
                    } else {
                        val = z;
                        return null;
                    }
                }
            }
        }});

        dialogBuilder.setPositiveButton("ok", pressok);
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();
    }

    //FOR SINGLE SELECT
    public void SingleSelectDialog(String head, int preset, final String[] values, DialogInterface.OnClickListener pressok) {

        amount = preset;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        dialogBuilder.setSingleChoiceItems(values, preset, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                amount = arg1;

            }
        });
        dialogBuilder.setPositiveButton("ok", pressok);
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }

    //for Single Select With Image
    public void SingleSelectWithImage(String head, ArrayList<TowardsBankAccount> bankval, DialogInterface.OnClickListener pressok) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        ArrayList<TowardsBankAccount> arr = new ArrayList<>();
        arr = bankval;
        if (bankval == null) {
            dialogBuilder.setMessage("Linked Banks.Linked funding Accounts");
        } else {
            final MultiSelectAdapter adapter = new MultiSelectAdapter(context, arr);
            dialogBuilder.setAdapter(adapter, pressok);
        }
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }

    //for Single Select With Image by ravi
    public void SelectSinglewithImage(String head, ArrayList<TowardsBankAccount> values, DialogInterface.OnClickListener pressok) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        // final MultiSelectAdapter adapter = new MultiSelectAdapter(context, valuefor_WeekAmount_Deduction, image);
        // dialogBuilder.setAdapter(adapter, pressok);
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();

    }


    //for Multi Select with checkbox
    public ArrayList<Integer> MultiSelectDialogCheck(String head, final ArrayList<goals_with_goalsof_particular_rule> goalsdata, final TextView goalcount, final boolean create) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialogBuilder.setTitle(head);
        ArrayList<goals_with_goalsof_particular_rule> copygoalsdata = new ArrayList<>();
        copygoalsdata = goalsdata;


        final MultiSelectAdapter adapter;
        if (create) {
            if (check++ == 0) {
                for (int i = 0; i < goalsdata.size(); i++) {
                    if (i == 0)
                        copygoalsdata.get(i).setRules_id(1);
                    else
                        copygoalsdata.get(i).setRules_id(0);
                }
            }

            adapter = new MultiSelectAdapter(context, copygoalsdata, 1);
        } else {
            adapter = new MultiSelectAdapter(context, copygoalsdata, 0);
        }

        dialogBuilder.setAdapter(adapter, null);

        dialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                adapter.getselectedList();
                //getting the count of selected valuefor_WeekAmount_Deduction
                finalselected_goals = new ArrayList<>();
                for (int i = 0; i < goalsdata.size(); i++) {
                    if (adapter.aa.get(i).equals(true)) {
                        finalselected_goals.add(goalsdata.get(i).getGoals_id());
                    }
                }

                //setting the textview according to sellected valuefor_WeekAmount_Deduction
                if (finalselected_goals.size() == 0)
                    goalcount.setText("Select Goal");
                else if (finalselected_goals.size() == 1) {
                    for (int i = 0; i < goalsdata.size(); i++) {
                        if (adapter.aa.get(i).equals(true))
                            goalcount.setText(goalsdata.get(i).getGoal_name());
                    }
                } else
                    goalcount.setText(finalselected_goals.size() + " Goals");


                //updating the data
                for (int i = 0; i < goalsdata.size(); i++) {
                    if (adapter.aa.get(i).equals(true))

                        goalsdata.get(i).setRules_id(1);
                    else
                        goalsdata.get(i).setRules_id(0);
                }

            }
        });
        dialogBuilder.setNegativeButton("Cancel", null);
        dialogBuilder.show();


        return finalselected_goals;

    }

    public int getVal() {

        return val;
    }

    public int getAmount() {

        return amount;
    }

  /*  ArrayList<Integer> finalselected_goals()
    {
    }*/


}
