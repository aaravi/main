package com.example.ravis.webs2.rules;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.goals.Goal_details;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_PERCENTAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;

public class Rule_freelancer extends AppCompatActivity {

    static Boolean firstclick_morethan = true, firstclick_save = true;

    CardView morethanlayout, savelayout, toyourlayout, towardd;

    Context context;

    Dialog builder;

    goalSelection objectForTowards;

    TextView dialog_title, dialogmessage, ofmorethan, saveoption, linkebank, counts, goalcount, freelancerPaageHeading, freelancerHeading;

    ImageView onback, ondelete;

    EditText dialogedittext;

    Button okbutton, cancelbutton, createfreelancerrulebutton;

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    AlertDialogs alert;

    ArrayList<Integer> goalid = new ArrayList<Integer>();

    Boolean create = true,isclicked=false;

    private int triggeringBankId, selected_saveAmount, field_savedPercentage;

    private int fieled_triggering_bank, ruleid = 20;

    private int check = 0, count = 0;


    private boolean isValueChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_b_freelancer);
        context = this;
        alert = new AlertDialogs(context);

        onback = (ImageView) findViewById(R.id.flback);
        ondelete = (ImageView) findViewById(R.id.fldelete);

        savelayout = (CardView) findViewById(R.id.flsave);
        morethanlayout = (CardView) findViewById(R.id.flmorethan);
        towardd = (CardView) findViewById(R.id.fltowardsgoal);
        toyourlayout = (CardView) findViewById(R.id.flfundedbank);
        objectForTowards = new goalSelection(context);
        ofmorethan = (TextView) findViewById(R.id.flmorethantext);
        saveoption = (TextView) findViewById(R.id.flsavetext);
        freelancerPaageHeading = (TextView) findViewById(R.id.flCardDescription);
        freelancerHeading = (TextView) findViewById(R.id.fltitle);
        goalcount = (TextView) findViewById(R.id.fltowardsgoaltext);
        linkebank = (TextView) findViewById(R.id.flfundedbanktext);

        createfreelancerrulebutton = (Button) findViewById(R.id.flbutton);

        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        bankval = new ArrayList<>();
        bankval = new SqlTransactions(Rule_freelancer.this).fundingBankAccount();
        goalsdata = new SqlTransactions(Rule_freelancer.this).getallgoalsforrule(ruleid);
        create = this.getIntent().getBooleanExtra("type", true);


        ruleid = this.getIntent().getIntExtra("ruleid", 63);


        //code for "Of more than custom dialog"
        morethanlayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isValueChanged = true;
                helperfunctions helperobject = new helperfunctions();

                createDialogBox();

                dialogedittext.setVisibility(View.VISIBLE);

                //component dialog box is always reused so
                // it is checked weather default hint is to be set or custom(recieved from edittext)
                dialogedittext.setHint(ofmorethan.getText());

                //user can enter max 4 digits
                helperobject.inputlimiter(dialogedittext, 4);

                dialog_title.setText("Of More Than");
                dialogmessage.setText("Amount(max ₹5,000)");
                builder.show();

                //validation so that user could enter only <= 5000
                helperobject.limittovalue(5000, 3, dialogedittext, okbutton);

                cancelbutton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        builder.cancel();
                        dialogedittext.setText("");
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        ofmorethan.setText("₹" + dialogedittext.getText().toString());
                        dialogedittext.setText("");
                        isValueChanged = true;
                        builder.cancel();
                    }
                });
            }
        });

        ondelete.setOnClickListener(deleteRule);


        ofmorethan.addTextChangedListener(new helperfunctions().PageHeadingChanger(ofmorethan, saveoption, null, freelancerPaageHeading, "freelancer"));
        saveoption.addTextChangedListener(new helperfunctions().PageHeadingChanger(ofmorethan, saveoption, null, freelancerPaageHeading, "freelancer"));


        savelayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isValueChanged = true;
                createDialogBox();
                helperfunctions helperobject = new helperfunctions();
                dialogedittext.setVisibility(View.VISIBLE);
                //component dialog box is always reused so
                // it is checked weather default hint is to be set or custom(recieved from edittext)
                if (firstclick_morethan) {
                    dialogedittext.setHint("30%");
                    firstclick_save = false;
                } else
                    dialogedittext.setHint(saveoption.getText());

                //user can enter max 3 digits
                helperobject.inputlimiter(dialogedittext, 3);
                dialogedittext.setHint(saveoption.getText().toString());

                dialog_title.setText("Save");
                dialogmessage.setText("Percent");
                builder.show();

                //validation so that user could enter only less than or equal to 5000
                helperobject.limittovalue(100, 2, dialogedittext, okbutton);
                cancelbutton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        builder.cancel();
                        dialogedittext.setText("");
                    }
                });
                okbutton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        saveoption.setText(dialogedittext.getText().toString() + "%");
                        isValueChanged = true;
                        dialogedittext.setText("");
                        builder.cancel();
                    }
                });
            }
        });
        toyourlayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isValueChanged = true;
                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();
                        linkebank.setText(x);
                        fieled_triggering_bank = bankval.get(which).getYodlee_id();
                    }
                });

            }
        });


        if (create) {
            ondelete.setVisibility(View.INVISIBLE);
            createfreelancerrulebutton.setText("Create Rule");
            freelancerHeading.setText("Create Freelancer Rule");
            goalcount.setText(goalsdata==null?"Select Goals":(goalsdata.get(0).getGoal_name()));
            towardd.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);


                }
            });

            createfreelancerrulebutton.setOnClickListener(createfreelancerRule);



        } else {
            ondelete.setVisibility(View.VISIBLE);
            ondelete.setOnClickListener(deleteRule);
            goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);
            createfreelancerrulebutton.setText("Save");
            loadAllValues();
            createfreelancerrulebutton.setOnClickListener(updateRule);
            freelancerPaageHeading.setText("Edit Rule");

            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);

            towardd.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isValueChanged = true;
                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });

        }

    }


    //This method created custo dialog box with edittext,Ok ,Cancel button--------------------------------------------------------------------
    private void createDialogBox() {

        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.z_customdialog_for_checkbox, null);

        builder = new Dialog(context);

        builder.setContentView(view);

        cancelbutton = (Button) builder.findViewById(R.id.custom_cancle);

        dialog_title = (TextView) builder.findViewById(R.id.custom_dialog_title);

        dialogmessage = (TextView) builder.findViewById(R.id.custom_dialog_message);

        dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);

        okbutton = (Button) builder.findViewById(R.id.custom_ok);

        okbutton.setEnabled(false);

        dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));

        dialogedittext.setTextColor(Color.parseColor("#000000"));

        okbutton.setTextColor(Color.parseColor("#b8d7e0"));

        dialogedittext.setHintTextColor(Color.parseColor("#808080"));

        dialogedittext.setHint("₹100");

        dialogedittext.setKeyListener(DigitsKeyListener.getInstance(false, false));
    }

    //when user presses Back button----------------------------------------------------------------------------------------------------------
    @Override
    public void onBackPressed() {

        // super.onBackPressed();

        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialog.setTitle("Discard Changes?");
        if (isValueChanged)
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Does nothing
                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {


                    Rule_freelancer.this.finish();
                }
            }).show();

        else

            finish();
    }

    //for edit freelancer rule all values are loaded into their respective fields-------------------------------------------------------------------
    private void loadAllValues() {

        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();//to move to fields

        //String a=c.getString(2)==null?"fine":"hello";

        ofmorethan.setText(c.getString(2).equals("") ? "0" : "₹" + c.getString(2));//save amount

        fieled_triggering_bank =
                c.getString(12) == null ? 0 : Integer.parseInt(c.getString(12));//id for triggering

        //   triggeringbank.setText(c.getString(13) == null ? "Please Link Your Bank" : c.getString(13));//triggering bank

        linkebank.setText(c.getString(13) == null ? "Please Link Your Bank"
                                                  : c.getString(13));//triggering  bank

        //    int_usingBank_Slected=c.getString(15)==null?0:Integer.parseInt(c.getString(15));

        saveoption.setText(c.getString(7).equals("") ? "0" : c.getString(7) + "%");//save percentage
    }

    //get all selected Fields from this rule and add them to ContentValue Object -----------------------------------
    private ContentValues getAllValuesFromFields() {

        ContentValues dbvalues = new ContentValues();

        dbvalues.put(RULE_NAME, "Freelancer Rule");

        dbvalues.put(RULE_IMAGE, R.drawable.img2);
        //  dbvalues.put(RULE_TRIGERRING_BANK, int_usingBank_Slected);

        dbvalues.put(RULE_TRIGERRING_BANK, fieled_triggering_bank);

        String stringValfromRounpUpValue = ofmorethan.getText().toString().replaceAll("[A-Z?i]|[₹]|[\\s]|", "").replace("\"", "");

        selected_saveAmount = Integer.parseInt(stringValfromRounpUpValue.replace("\"", ""));

        dbvalues.put(RULE_SAVE_AMOUNT, selected_saveAmount);

        String stringSavePercentage = saveoption.getText().toString().replaceAll("[%]", "").replace("\"", "");

        field_savedPercentage = Integer.parseInt(stringSavePercentage.replace("\"", ""));

        dbvalues.put(RULE_PERCENTAGE, field_savedPercentage);

        return dbvalues;
    }

    //Listner when app has to create new rule------------------------------------------------------------------------------------------
    private Button.OnClickListener createfreelancerRule = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            isValueChanged = true;

            objectForTowards.getGoalid(isclicked,goalsdata);

            long result = new SqlTransactions(context).insertRule(getAllValuesFromFields());

            if (result != -1) {

                Intent in = new Intent(Rule_freelancer.this,helperfunctions.AfterNewRuleReurnToClass);

                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                in.putExtra("position", helperfunctions.helperGoalPosition);

                finish();

                startActivity(in);

            } else

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();

            new SqlTransactions(context).insertmappingcreateRule(objectForTowards.goalid);
        }
    };

    //Listner When this actvity acts as update Rule Activity--------------------------------------------------------------------------------
    Button.OnClickListener updateRule = new View.OnClickListener() {

        ArrayList<Integer> finalSelected_goals = new ArrayList<>();

        @Override
        public void onClick(View v) {

            isValueChanged = true;

            if (objectForTowards.alert.finalselected_goals == null) {
                for (int i = 0; i < goalsdata.size(); i++) {
                    if (goalsdata.get(i).getRules_id() == ruleid)
                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }
            } else {
                finalSelected_goals = objectForTowards.alert.finalselected_goals;
            }

            new SqlTransactions(context).updateRuleWithMappings(getAllValuesFromFields(), ruleid, alert.finalselected_goals);
            finish();

        }
    };


    //If user decides to delete rule ,this method invokes---------------------------------------------------------------------------------------
    View.OnClickListener deleteRule = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

            dialog.setTitle("Confirm Delete");

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    long result=new SqlTransactions(context).deleteRule(ruleid);
                    Toast.makeText(context, result != -1 ? "Rule Deleted"
                                                    : "Error deleting rule", Toast.LENGTH_SHORT).show();
                    finish();

                }
            }).show();

        }
    };

}