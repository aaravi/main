package com.example.ravis.webs2.pojo;

import android.content.Context;

import com.example.ravis.webs2.DatabaseHandle.DatabaseCreator;
import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.goals.Goal_Allgoals;
import com.example.ravis.webs2.R;
import java.util.ArrayList;

/**
 * Created by ravis on 6/13/2017.
 */

public class NewGoalPojo {
    public ArrayList<Goals> arr;
    SqlTransactions tempobj;
    DatabaseCreator creator;
    Context context;
    public NewGoalPojo(Context context)
    {
        this.context=context;
        creator=new DatabaseCreator(context);
        tempobj= new SqlTransactions(context);
        arr=tempobj.arraylistallGoalsfromDb();
        tempobj.closeDatabase();
    }
    public NewGoalPojo(int x){
        arr=new ArrayList<>();
        Goals obj1;
        obj1=new Goals("Do Something",String.valueOf(R.drawable.goal_do_something));
        arr.add(obj1);
        obj1=new Goals("Go Somwhere",String.valueOf(R.drawable.goal_go_somewhere));
        arr.add(obj1);
        obj1=new Goals("Get Something",String.valueOf(R.drawable.goal_get_something));
        arr.add(obj1);
        obj1=new Goals("Just Start Saving",String.valueOf(R.drawable.goal_just_start_saving));
        arr.add(obj1);
        obj1=new Goals("Pay Off Debts",String.valueOf(R.drawable.goal_pay_off_debts));
        arr.add(obj1);
    }
}
