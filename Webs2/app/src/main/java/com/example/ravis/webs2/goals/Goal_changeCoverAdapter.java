package com.example.ravis.webs2.goals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.CustomCover;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Kshitij Mittal on 03-07-2017.
 */

public class Goal_changeCoverAdapter extends RecyclerView.Adapter<Goal_changeCoverAdapter.MyViewHolder>{


    private List<CustomCover> ImageList;
    Context context;
    CustomCover selectedCover;
    String searchkey;
    private static final String ChangeImageKey="imageurl";

    public Goal_changeCoverAdapter(Context context, List<CustomCover> ImageList,String searchkey) {
        this.context=context;
        this.ImageList = ImageList;
        this.searchkey=searchkey;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView im;

        public MyViewHolder(View view) {
            super(view);
            im=(ImageView) view.findViewById(R.id.imagein);
            im.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent();
                    i.putExtra(ChangeImageKey,ImageList.get(getAdapterPosition()).getLink());
                    i.putExtra("searchkey",searchkey);
                    ((Activity)context).setResult(Activity.RESULT_OK,i);
                    ((Activity)context).finish();
                }
            });

        }

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.goal_z_changecoverbox, parent, false);
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        selectedCover = ImageList.get(position);
        Picasso mp=Picasso.with(context);
                mp.load(selectedCover.getThumbnailLink())
                .networkPolicy(NetworkPolicy.NO_STORE)
                .placeholder(R.drawable.ic_imageholder)
                .error(R.drawable.ic_errorimage)
                .into(holder.im);
    }


    @Override
    public int getItemCount() {
        return ImageList.size();
    }


}
