package com.example.ravis.webs2.pojo;

import com.example.ravis.webs2.R;

import java.util.ArrayList;

/**
 * Created by ravis on 6/7/2017.
 * Dummy data class for rules
 */

public class newRulePojo {
    public int img;
    public String title;
    public String desc;
    public String color;

    public int getImg() {
        return img;
    }
    public String getTitle() {
        return title;
    }
    public String getDesc() {
        return desc;
    }
    public String getColor() {
        return color;
    }
    public newRulePojo(int img, String title, String desc, String color) {
        this.img = img;
        this.title = title;
        this.desc = desc;
        this.color = color;
    }

    public static ArrayList<newRulePojo> DummyRules() {
        ArrayList<newRulePojo> list = new ArrayList<>();
        newRulePojo obj = new newRulePojo(R.drawable.rule_roundup_icon,  "Round up Rule", "Save the change every time you swipe the card.", "#00b898");
        list.add(obj);
        obj = new newRulePojo(R.drawable.rule_spendless_icon, "Spend Less Rule", "Set a budget. come in under. save the difference", "#2e294e");
        list.add(obj);
        obj = new newRulePojo(R.drawable.rule_guiltypleasure_icon, "Guilty Pleasure Rule", "Put a little in saving whenever you buy the stuff you're trying to resist", "#673ab7");
        list.add(obj);
        obj = new newRulePojo(R.drawable.rule_setforget_icon, "Set & Forget Rule", "Set up daily, weekly or monthly deposits to make savings automatic", "#ffbc00");
        list.add(obj);
        obj = new newRulePojo(R.drawable.rule_52week_icon, "52 week Rule", "Save ₹1 week 1, ₹2 week 2 and so on for 52 weeks", "#db5461");
        list.add(obj);
        obj = new newRulePojo(R.drawable.rule_freelancer_icon, "Freelancer Rule", "Set aside 30% for tax day every time you get paid", "#2e294e");
        list.add(obj);
    return list;}


}


