package com.example.ravis.webs2.pojo;

/**
 * Created by ravis on 6/30/2017.
 */

public class TowardsBankAccount  {
    int yodlee_id=0;
    String bankName=null;
    String bankIcon=null;

    public int getYodlee_id() {
        return yodlee_id;
    }

    public void setYodlee_id(int yodlee_id) {
        this.yodlee_id = yodlee_id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankIcon() {
        return bankIcon;
    }

    public void setBankIcon(String bankIcon) {
        this.bankIcon = bankIcon;
    }
}
