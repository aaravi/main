package com.example.ravis.webs2.goals;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.NewGoalPojo;

/**
 * Created by ravis on 6/13/2017.
 */

public class Goal_createNew extends AppCompatActivity {
RecyclerView goalsRecyclerView;
    Goal_createNewAdapter selectGoalsRviewAdapter;
CardView goalsCardView;
    RecyclerView.LayoutManager manager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goal_c_createnew);
        NewGoalPojo pojo=new NewGoalPojo(5);

        ImageView onback=(ImageView)findViewById(R.id.onback);

        onback.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {onBackPressed();
            }
        });

        goalsRecyclerView=(RecyclerView)findViewById(R.id.selectGoalsRecyclerView);
        goalsCardView=(CardView)findViewById(R.id.selectgoalsCardVIew);
        manager=new LinearLayoutManager(this);
        goalsRecyclerView.setLayoutManager(manager);
        goalsRecyclerView.setNestedScrollingEnabled(false);
        selectGoalsRviewAdapter=new Goal_createNewAdapter(this,pojo);
        goalsRecyclerView.setAdapter(selectGoalsRviewAdapter);



    }
}
