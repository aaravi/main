package com.example.ravis.webs2.goals;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravis.webs2.pojo.NewGoalPojo;
import com.example.ravis.webs2.R;

/**
 * Created by ravis on 6/13/2017.
 */

public class Goal_createNewAdapter extends RecyclerView.Adapter<Goal_createNewAdapter.ViewHolder> {
    Context context;
    NewGoalPojo pojo;

    public Goal_createNewAdapter(Context context, NewGoalPojo pojo) {
        this.context = context;
        this.pojo = pojo;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardViewImage;
        public TextView textView;
        public CardView selectgoals_cardview;

        public ViewHolder(View itemView) {
            super(itemView);
            cardViewImage = (ImageView) itemView.findViewById(R.id.cardviewimage);
            textView = (TextView) itemView.findViewById(R.id.textongoals);
            selectgoals_cardview = (CardView) itemView.findViewById(R.id.selectgoalsCardVIew);
        }
    }

    @Override
    public Goal_createNewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.goal_c_createnewcard, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.textView.setText(pojo.arr.get(position).getLocal_goal_name());
        holder.cardViewImage.setImageResource(Integer.valueOf(pojo.arr.get(position).getLocal_imagelink()));
        holder.selectgoals_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent im = new Intent(context, Goal_createEdit.class);
                im.putExtra(context.getString(R.string.intent), context.getString(R.string.create));
                im.putExtra("imageurl", Integer.valueOf(pojo.arr.get(holder.getAdapterPosition()).getLocal_imagelink()));
                context.startActivity(im);
                ((Activity)context).finish();
            }
        });
    }


    @Override
    public int getItemCount() {
        return pojo.arr.size();
    }
}
