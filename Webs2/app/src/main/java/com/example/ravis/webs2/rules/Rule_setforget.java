package com.example.ravis.webs2.rules;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.goals.Goal_details;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_DURATION;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;

public class Rule_setforget extends AppCompatActivity {

    CardView saveOption, duration, fundedaccount, goalselection;

    ImageView ondelete,onback;

    ContentValues dbvalues;

    goalSelection objectForTowards;

    final String[] values = {"week", "month", "year"};

    final String[] bankvalue = {"Bank 1", "Bank 2", "Bank 3", "Bank 4", "Bank 5"};

    final String[] goalvalue = {"Goal 1", "Goal 2", "Goal 3", "Goal 4", "Goal 5"};

    boolean[] selected = {false, true, false, false, true};

    int[] image = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5};

    int field_fundedBank_Selected = 0;

    int set = 0, limit = 500, preset = 30, check = 0;

    AlertDialogs alert;

    Button createforgetrulebutton;

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    TextView sffundbank;

    TextView  goalcount;

    Boolean create = true,isclicked=false;;

    static Context context;


    TextView sfsaveamount, sfper,sfpageHeading,sfCardDescription;

    private int ruleid = 0;

    private int field_fundedBank, countt = 0, saveamount = 20;

    private boolean isEdited=false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_b_setforget);
        context = this;
        objectForTowards = new goalSelection(context);
        bankval = new SqlTransactions(context).fundingBankAccount();
        create=this.getIntent().getBooleanExtra("type",true);
        ruleid = this.getIntent().getIntExtra("ruleid",2);
        goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);

        onback = (ImageView) findViewById(R.id.sfback);
        ondelete = (ImageView) findViewById(R.id.sfdelete);

        duration = (CardView) findViewById(R.id.sfduration);
        fundedaccount = (CardView) findViewById(R.id.sffundedbank);
        goalselection = (CardView) findViewById(R.id.sftowardsgoal);
        saveOption = (CardView) findViewById(R.id.sfsave);

        sfper = (TextView) findViewById(R.id.sfdurationtext);
        goalcount = (TextView) findViewById(R.id.sftowardsgoaltext);
        sffundbank = (TextView) findViewById(R.id.sffundbanktext);
        sfpageHeading=(TextView)findViewById(R.id.sftitle);
        sfsaveamount = (TextView) findViewById(R.id.sfsavetext);

        createforgetrulebutton = (Button) findViewById(R.id.sfbutton);

        sfCardDescription=(TextView)findViewById(R.id.sfCardDescription);


        onback.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {onBackPressed();
            }
        });
        alert = new AlertDialogs(context);
        dbvalues = new ContentValues();
        dbvalues.put(RULE_NAME, "Create & Forget Rule");
        dbvalues.put(RULE_IMAGE, R.drawable.img2);
        sfper.addTextChangedListener(new helperfunctions().PageHeadingChanger(sfsaveamount,sfper,null,sfCardDescription,"setforget"));
        sfsaveamount.addTextChangedListener(new helperfunctions().PageHeadingChanger(sfsaveamount,sfper,null,sfCardDescription,"setforget"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("response"));

        init();


    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder dialog=new AlertDialog.Builder(context,R.style.MyDialogTheme);
        dialog.setTitle("Discard Changes?");
        if(isEdited)
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {


                @Override
                public void onClick(DialogInterface dialog, int which)
                {

                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener()
            {


                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    // rounduprule.this.onBackPressed();//long result=new SqlTransactions(context).deleteRule(ruleid)
                    finish();
                }
            }).show();
        else
            finish();
    }

    public void init()
    {



        saveOption.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
                alert.editTextField("SAVE", "Max limit = ₹" + String.valueOf(limit), preset, limit, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        sfsaveamount.setText("₹" + String.valueOf(alert.getVal()));
                        saveamount = alert.getVal();
                        dbvalues.put(RULE_SAVE_AMOUNT, alert.getVal());
                    }
                });
            }
        });




        duration.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
                alert.SingleSelectDialog("Duration", set, values, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        set = alert.getAmount();
                        dbvalues.put(RULE_DURATION, set);
                        sfper.setText(values[set]);
                    }
                });
            }
        });




        fundedaccount.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                isEdited=true;
                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        String x = bankval.get(which).getBankName();
                        sffundbank.setText(x);
                        field_fundedBank = bankval.get(which).getYodlee_id();
                    }
                });

            }
        });
        if (create)
        {
            createforgetrulebutton.setText("Create Rule");
            sfpageHeading.setText("Create Set & Forget Rule");
            ondelete.setVisibility(View.INVISIBLE);
            createforgetrulebutton.setOnClickListener(createforgetrulelistener);
            goalcount.setText(goalsdata==null?"Select Goals":(goalsdata.get(0).getGoal_name()));
            goalselection.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    isEdited=true;
                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);
                }
            });

        } else
        {
            sfpageHeading.setText("Edit Rule");
            ondelete.setVisibility(View.VISIBLE);
            ondelete.setOnClickListener(deleteRule);
            createforgetrulebutton.setOnClickListener(UpdateRule);
            createforgetrulebutton.setText("Save");
           loadValuestoTheirFields();
            goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);

            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);
            goalselection.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v)
                {
                    isEdited=true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });
        }

    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver()
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {

            selected = intent.getBooleanArrayExtra("isselected");
        }
    };


    private ContentValues getValuesFromFields()
    {

        ContentValues dbvalues = new ContentValues();
        dbvalues.put(RULE_NAME, "Set & Forget Rule");
        dbvalues.put(RULE_IMAGE, R.drawable.img2);
        dbvalues.put(RULE_FUNDED_BANK, field_fundedBank);
        dbvalues.put(RULE_SAVE_AMOUNT, saveamount);
        dbvalues.put(RULE_DURATION, set);
        return dbvalues;
    }

    private Button.OnClickListener createforgetrulelistener = new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            long insert = new SqlTransactions(context).insertRule(getValuesFromFields());
            Toast.makeText(context,""+insert,Toast.LENGTH_SHORT).show();
            objectForTowards.getGoalid(isclicked,goalsdata);

            new SqlTransactions(context).insertmappingcreateRule(objectForTowards.goalid);

            if (insert != -1)
            {
                Toast.makeText(context, "Done" +insert, Toast.LENGTH_SHORT).show();
                Intent in = new Intent(context, helperfunctions.AfterNewRuleReurnToClass);
                finish();
                startActivity(in);
            } else
                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
    };

    private void loadValuestoTheirFields()
    {
        //createrounduprulebutton.setText("Save");
        //  alert.finalselected_goals=new SqlTransactions(context).getallgoalsforrule(ruleid);

        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();//to move to fields

        sfsaveamount.setText(c.getString(2).equals("") ? "0" : "₹" + c.getString(2));//save amount

        field_fundedBank=c.getString(15) == null ? 0 : Integer.parseInt(c.getString(15));//id for funded bank

        sffundbank.setText(c.getString(14) == null ? "Please Link Your Bank" : c.getString(14));//funded bank name

        String duration = c.getString(6) == null ? "0" : c.getString(6);
        String finalValInTextView = "";
        if (duration.equals("0"))
        {
            set = 0;
            finalValInTextView = "Week";
        } else if (duration.equals("1"))
        {
            set = 1;
            finalValInTextView = "Month";
        } else
        {
            set = 2;
            finalValInTextView = "Year";
        }

        sfper.setText(finalValInTextView);


    }

    View.OnClickListener UpdateRule = new View.OnClickListener()
    {
        ArrayList<Integer> finalSelected_goals=new ArrayList<>();

        @Override
        public void onClick(View v)
        {
            if(objectForTowards.alert.finalselected_goals==null)
            {
                for(int i=0;i<goalsdata.size();i++)
                {
                    if (goalsdata.get(i).getRules_id() == ruleid)
                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }
            }
            else
            {
                finalSelected_goals=objectForTowards.alert.finalselected_goals;
            }

            new SqlTransactions(context).updateRuleWithMappings(getValuesFromFields(), ruleid, finalSelected_goals);
        }
    };


    View.OnClickListener deleteRule=new View.OnClickListener()
    {

        @Override
        public void onClick(View v)
        {

            AlertDialog.Builder dialog=new AlertDialog.Builder(context,R.style.MyDialogTheme);
            dialog.setTitle("Confirm Delete");
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener()
            {

                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    long result=new SqlTransactions(context).deleteRule(ruleid);
                    Toast.makeText(context,result!=-1?"Rule Deleted":"Error deleting rule",Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).show();
        }
    };

}