package com.example.ravis.webs2.DatabaseHandle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;


public class DatabaseCreator extends SQLiteOpenHelper{
    //public static String mapping;
    SQLiteDatabase db;
    //database name
    public static final String DATABASE_NAME="capgoalrules.db";

    //table name
    public static final String TABLE_GOALS="Goals";
    public static final String TABLE_RULES="Rules";
    public static final String TABLE_MAPPING="mapping";
    public static final String TABLE_ACCOUNTS = "accounts";

    //Goals column names
    public static final String GOAL_ID="goalid"; //mapping column name
    public static final String GOAL_NAME="goalname";
    public static final String GOAL_COLLECTED_AMOUNT="collectedamount";
    public static final String GOAL_TOTAL_AMOUNT="totalamount";
    public static final String GOAL_IMAGE="images";


    //Rules column names
    public static final String  RULE_ID="ruleid"; //mapping column name
    public static final String  RULE_NAME="rulename";
    public static final String  RULE_SAVE_AMOUNT="saveamount";
    public static final String  RULE_TRIGERRING_BANK="triggeringbank";
    public static final String  RULE_FUNDED_BANK ="fundedbank";
    public static final String  RULE_MERCHANT="merchant";
    public static final String  RULE_DURATION="duration";
    public static final String  RULE_PERCENTAGE="savepercentage";
    public static final String  RULE_ORDER="weekorder";
    public static final String  RULE_SAVED_AMOUNT="savedamount";
    public static final String  RULE_IS_PAUSED="ispaused";
    public static final String  RULE_IMAGE="ruleimages";

    //b Accounts data -------------------------------------------------------
    public static final String COLUMN_ACCOUNTS_id = "id";
    public static final String COLUMN_ACCOUNTS_CONTAINER = "CONTAINER";
    public static final String COLUMN_ACCOUNTS_accountName = "accountName";
    public static final String COLUMN_ACCOUNTS_accountStatus = "accountStatus";
    public static final String COLUMN_ACCOUNTS_accountNumber = "accountNumber";
    public static final String COLUMN_ACCOUNTS_isAsset = "isAsset";
    public static final String COLUMN_ACCOUNTS_balance_amount = "balance_amount";
    public static final String COLUMN_ACCOUNTS_balance_currency = "balance_currency";
    public static final String COLUMN_ACCOUNTS_yodlee_id = "yodlee_id";
    public static final String COLUMN_ACCOUNTS_lastUpdated = "lastUpdated";
    public static final String COLUMN_ACCOUNTS_includeInNetWorth = "includeInNetWorth";
    public static final String COLUMN_ACCOUNTS_providerId = "providerId";
    public static final String COLUMN_ACCOUNTS_providerName = "providerName";
    public static final String COLUMN_ACCOUNTS_currentBalance_amount = "currentBalance_amount";
    public static final String COLUMN_ACCOUNTS_currentBalance_currency = "currentBalance_currency";
    public static final String COLUMN_ACCOUNTS_accountType = "accountType";
    public static final String COLUMN_ACCOUNTS_refresh_statusMessage = "refresh_statusMessage";
    public static final String COLUMN_ACCOUNTS_status = "status";
    public static final String COLUMN_ACCOUNTS_icon_url = "icon_url";

    public static final String TABLE_CREATE_ACCOUNT = "create table "
            + TABLE_ACCOUNTS + "(" + COLUMN_ACCOUNTS_id
            + " INTEGER PRIMARY KEY, "
            + COLUMN_ACCOUNTS_CONTAINER + " TEXT, "
            + COLUMN_ACCOUNTS_accountName + " TEXT, "
            + COLUMN_ACCOUNTS_accountStatus + " TEXT, "
            + COLUMN_ACCOUNTS_accountNumber + " INTEGER, "
            + COLUMN_ACCOUNTS_isAsset + " INTEGER, "
            + COLUMN_ACCOUNTS_balance_amount + " REAL, "
            + COLUMN_ACCOUNTS_balance_currency + " TEXT, "
            + COLUMN_ACCOUNTS_yodlee_id + " INTEGER, "
            + COLUMN_ACCOUNTS_lastUpdated + " NUMERIC, "
            + COLUMN_ACCOUNTS_includeInNetWorth + " INTEGER, "
            + COLUMN_ACCOUNTS_providerId + " INTEGER, "
            + COLUMN_ACCOUNTS_providerName + " TEXT, "
            + COLUMN_ACCOUNTS_currentBalance_amount + " REAL, "
            + COLUMN_ACCOUNTS_currentBalance_currency + " TEXT, "
            + COLUMN_ACCOUNTS_accountType + " TEXT, "
            + COLUMN_ACCOUNTS_refresh_statusMessage + " TEXT, "
            + COLUMN_ACCOUNTS_status + " TEXT, "
            + COLUMN_ACCOUNTS_icon_url + " TEXT)";

    public static final String CREATE_GOAL_TABLE=
            "CREATE TABLE " +TABLE_GOALS+ "( "
                    +GOAL_ID+               " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                    +GOAL_NAME+             " TEXT, "
                    +GOAL_COLLECTED_AMOUNT+ " INTEGER, "
                    +GOAL_TOTAL_AMOUNT+     " INTEGER, "
                    +GOAL_IMAGE+            " INTEGER ); ";

    public static final String CREATE_RULE_TABLE=
            "CREATE TABLE " +TABLE_RULES+ "( "
                    +RULE_ID+               " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                    +RULE_NAME+             " TEXT, "
                    +RULE_SAVE_AMOUNT+      " INTEGER, "
                    +RULE_TRIGERRING_BANK+  " TEXT, "
                    + RULE_FUNDED_BANK +    " TEXT, "
                    +RULE_MERCHANT+         " TEXT, "
                    +RULE_DURATION+         " INTEGER, "
                    +RULE_PERCENTAGE+       " INTEGER, "
                    +RULE_ORDER+            " INTEGER, "
                    +RULE_SAVED_AMOUNT+     " INTEGER, "
                    +RULE_IS_PAUSED+        " INTEGER, "
                    +RULE_IMAGE+            " INTEGER  ); ";



    public static final String CREATE_MAPPING_TABLE=
            "CREATE TABLE " +TABLE_MAPPING+ "("
                    +GOAL_ID+               " INTEGER, "
                    +RULE_ID+               " INTEGER, "
                    +"PRIMARY KEY ("+GOAL_ID+","+RULE_ID+"), "
                    +"FOREIGN KEY ("+GOAL_ID+") REFERENCES " +TABLE_GOALS+"("+GOAL_ID+") ON DELETE CASCADE, "
                    +"FOREIGN KEY ("+RULE_ID+") REFERENCES " +TABLE_RULES+"("+RULE_ID+") ON DELETE CASCADE ); ";

    public static final String CREATE_MAPPING_TABLE2=
            "CREATE TABLE " +TABLE_MAPPING+ "("
                    +GOAL_ID+               " INTEGER REFERENCES " +TABLE_GOALS+"("+GOAL_ID+") ON DELETE CASCADE, "
                    +RULE_ID+               " INTEGER REFERENCES " +TABLE_RULES+"("+RULE_ID+") ON DELETE CASCADE, "
                    +"PRIMARY KEY ("+GOAL_ID+","+RULE_ID+")); ";


    public DatabaseCreator(Context context) {
        super(context, DATABASE_NAME, null, 1);
        open();
    }
    public SQLiteDatabase open()
    {
        db=this.getWritableDatabase();
        return  db;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            db.setForeignKeyConstraintsEnabled(true);
        }else{ db.execSQL("PRAGMA foreign_keys=ON;");}
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_ACCOUNT);
        db.execSQL(CREATE_GOAL_TABLE);
        db.execSQL(CREATE_RULE_TABLE);
        db.execSQL("PRAGMA foreign_keys=ON;");
        db.execSQL(CREATE_MAPPING_TABLE2);

        db.execSQL("INSERT INTO accounts" +
                "(providerName,yodlee_id,icon_url) " +
                "VALUES ('pnb',20,'https://i.imgur.com/tGbaZCY.jpg')");
        db.execSQL("INSERT INTO accounts" +
                "(providerName,yodlee_id,icon_url) " +
                "VALUES ('icici',21,'https://i.imgur.com/tGbaZCY.jpg')");
        db.execSQL("INSERT INTO accounts" +
                "(providerName,yodlee_id,icon_url) " +
                "VALUES ('cbi',22,'https://i.imgur.com/tGbaZCY.jpg')");


        /*db.execSQL("INSERT INTO goals" +
                "(goalname,collectedamount,totalamount,images) " +
                "VALUES ('ferrari',12500,20000,'http://buyersguide.caranddriver.com/media/assets/submodel/6873.jpg')");

        db.execSQL("INSERT INTO goals" +
                "(goalname,collectedamount,totalamount,images) " +
                "VALUES ('flat',80000,120000,'http://www.book-a-flat.com/images/paris-salon-2.jpg')");

        db.execSQL("INSERT INTO goals" +
                "(goalname,collectedamount,totalamount,images) " +
                "VALUES ('london',20000,200000,'http://www.telegraph.co.uk/content/dam/video_previews/x/5/x5cgi0ode66q6vuxezqmehmexwer6bt-xlarge.jpg')");


        db.execSQL("INSERT INTO goals" +
                "(goalname,collectedamount,totalamount,images) " +
                "VALUES ('bike',20,20000,'https://s-media-cache-ak0.pinimg.com/736x/51/e2/3b/51e23b2c0d37dda46571af26818c0fd6--ducati--ducati-motorcycles.jpg')");



        db.execSQL("INSERT INTO RULES" +
                "(rulename,saveamount,ruleimages,fundedbank,ispaused) " +
                "VALUES ('Round Up Rule',20,'https://i.imgur.com/tGbaZCY.jpg','cbi',0)");

        db.execSQL("INSERT INTO RULES" +
                "(rulename,saveamount,ruleimages,fundedbank,ispaused) " +
                "VALUES ('Spend Less Rule',200,'http://www.readersdigest.ca/wp-content/uploads/2011/01/4-ways-cheer-up-depressed-cat.jpg','pnb',0)");

        db.execSQL("INSERT INTO RULES" +
                "(rulename,saveamount,ruleimages,fundedbank,ispaused) " +
                "VALUES ('Set & Forget Rule',200,'https://i.imgur.com/tGbaZCY.jpg','icici',0)");

        db.execSQL("INSERT INTO RULES" +
                "(rulename,saveamount,ruleimages,fundedbank,ispaused) " +
                "VALUES ('create forget',20000,'http://www.readersdigest.ca/wp-content/uploads/2011/01/4-ways-cheer-up-depressed-cat.jpg','cbi',0)");




        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (1,1)");

        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (1,2)");
        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (1,3)");

        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (2,1)");

        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (2,3)");
        db.execSQL("INSERT INTO mapping" +
                "(goalid,ruleid) " +
                "VALUES (3,3)");
*/

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_GOALS);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_RULES);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_MAPPING);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
        super.onOpen(db);
    }
}
