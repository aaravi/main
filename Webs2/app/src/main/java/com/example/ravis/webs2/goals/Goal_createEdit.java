package com.example.ravis.webs2.goals;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.Goals;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_COLLECTED_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_TOTAL_AMOUNT;

public class Goal_createEdit extends AppCompatActivity {
    ImageView coverImage, ondelete, onback;
    TextView chooserules;
    EditText goalname, goalamount;
    Button check;
    LinearLayout selectImage;
    TextInputLayout goalnamelayout, goalamountlayout;
    String selectedImage = "";
    SqlTransactions sqt;
    Goals editGoal;
    boolean wht, isEdited = false;
    ArrayList<Integer> selectedRules = new ArrayList<>();
    static int idorimage = -1;
    private static final int onChooseRule = 1;
    private static final int onChangeImage = 3;
    private static final String ChangeImageKey = "imageurl";
    private static final String selectedRuleKey="selectedrules";
    private static final String goalId = "goalid";
    private static final String isedited = "isedited";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goal_d_createeditgoal);
        context=this;
        Intent in = getIntent();
        preset();
        String tnt = in.getStringExtra(getString(R.string.intent));
        if (tnt.equals(getString(R.string.create))) {
            idorimage = in.getIntExtra(ChangeImageKey, -1);
            wht = false;
            createFn();
        } else if (tnt.equals(getString(R.string.edit))) {
            idorimage = in.getIntExtra(goalId, -1);
            wht = true;
            editFn();
        }

        onback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void preset() {
        goalnamelayout = (TextInputLayout) findViewById(R.id.goalnamelayout);
        goalamountlayout = (TextInputLayout) findViewById(R.id.goalamountlayout);

        sqt = new SqlTransactions(this);
        onback = (ImageView) findViewById(R.id.back);
        check = (Button) findViewById(R.id.check);
        goalname = (EditText) findViewById(R.id.goalname);
        coverImage = (ImageView) findViewById(R.id.coverImage);
        goalamount = (EditText) findViewById(R.id.amountneeded);
        selectImage = (LinearLayout) findViewById(R.id.selectImage);
        chooserules = (TextView) findViewById(R.id.rules);
        goalname.setSingleLine();
        goalname.setImeOptions(EditorInfo.IME_ACTION_NEXT);


        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent image = new Intent(Goal_createEdit.this, Goal_changeCover.class);
                startActivityForResult(image, onChangeImage);
            }
        });

    }

    private void createFn() {
        coverImage.setImageResource(idorimage);
        TextView title = (TextView) findViewById(R.id.goaltitle);
        title.setText("Create Goal");
        check.setText("CREATE");
        check.setOnClickListener(createCheck);
        chooserules.setOnClickListener(CreateChooseRule);

    }

    private void editFn() {
        ondelete = (ImageView) findViewById(R.id.delete);
        editGoal = sqt.goalToedit(idorimage);
        selectedImage = editGoal.getLocal_imagelink();
        if (Character.isDigit(selectedImage.charAt(0))) {
            coverImage.setImageResource(Integer.valueOf(selectedImage));
        }else {
            Picasso.with(this)
                    .load(selectedImage)
                    .fit()
                    .centerCrop()
                    .into(coverImage);
        }
        goalname.setText(editGoal.getLocal_goal_name());
        goalname.setSelection(editGoal.getLocal_goal_name().length());
        goalamount.setText(String.valueOf(editGoal.getLocal_total_amount()));
        goalamount.setSelection(String.valueOf(editGoal.getLocal_total_amount()).length());
        ondelete.setVisibility(View.VISIBLE);
        check.setOnClickListener(editCheck);
        chooserules.setOnClickListener(EditChooseRule);
        ondelete.setOnClickListener(delete);

    }


    //listener for create
    View.OnClickListener createCheck = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkChange()) {
                if (selectedImage.equals("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(Goal_createEdit.this, R.style.MyDialogTheme);
                    alert.setTitle("Want to change Cover");
                    alert.setNegativeButton("SKIP", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            selectedImage = String.valueOf(idorimage);
                            ContentValues dbvalues = bundleup();
                            sqt.insertGoal(dbvalues,selectedRules);
                            finish();

                        }
                    });
                    alert.setPositiveButton("Change Cover", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent image = new Intent(Goal_createEdit.this, Goal_changeCover.class);
                            startActivityForResult(image, onChangeImage);
                        }
                    });
                    alert.show();
                } else {
                    ContentValues dbvalues = bundleup();
                    sqt.insertGoal(dbvalues, selectedRules);
                    finish();
                }
            }
        }
    };
    //listener for update
    View.OnClickListener editCheck = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (checkChange()) {
                ContentValues dbvalues = bundleup();
                sqt.updateGoal(idorimage, dbvalues, selectedRules, isEdited);
                finish();
            }
        }
    };

    //listeners to create choose Rules
    View.OnClickListener CreateChooseRule = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Goal_createEdit.this, Goal_chooseRule.class);
            i.putIntegerArrayListExtra(selectedRuleKey,selectedRules);
            i.putExtra(isedited,isEdited);
            startActivityForResult(i, onChooseRule);
        }
    };
    //listeners to edit choose Rules
    View.OnClickListener EditChooseRule = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(Goal_createEdit.this, Goal_chooseRule.class);
            i.putIntegerArrayListExtra(selectedRuleKey,selectedRules);
            i.putExtra(isedited,isEdited);
            i.putExtra(goalId, editGoal.getLocal_goals_id());
            startActivityForResult(i, onChooseRule);
        }
    };

    //listener for delete
    View.OnClickListener delete = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(Goal_createEdit.this, R.style.MyDialogTheme);
            alert.setTitle("are you Sure to DELETE ?");
            alert.setIcon(R.drawable.k_delete_black);
            alert.setNegativeButton("NO", null);
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sqt.deleteGoal(idorimage);
                    finish();
                    Intent in=new Intent(context,Goal_Allgoals.class );
                    startActivity(in);
                }
            });
            alert.show();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == onChangeImage) {
            if (resultCode == Activity.RESULT_OK) {
                selectedImage = data.getStringExtra(ChangeImageKey);

                Picasso.with(this)
                        .load(selectedImage)
                        .fit()
                        .placeholder(R.drawable.ic_imageholder)
                        .error(R.drawable.ic_errorimage)
                        .into(coverImage);
                if(goalname.getText().toString().equals("")) {
                    String searchkey = data.getStringExtra("searchkey");
                    searchkey=searchkey.replaceAll("[^a-zA-Z ]","");
                    goalname.setText(searchkey);
                    goalname.setSelection(searchkey.length());
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        } else if (requestCode == onChooseRule) {
            if (resultCode == Activity.RESULT_OK) {
                isEdited = true;
                selectedRules = data.getIntegerArrayListExtra(selectedRuleKey);
            } else if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (checkdatachange(wht)) {
            AlertDialog.Builder alert = new AlertDialog.Builder(Goal_createEdit.this, R.style.MyDialogTheme);
            alert.setTitle("Discard Changes ?");
            alert.setNegativeButton("NO", null);
            alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.show();
        } else {
            finish();
        }

    }

    private boolean checkChange() {
        boolean ok = true;
        if (goalname.getText().toString().trim().equals("")) {
            goalnamelayout.setError("Enter Valid Goal Name");
            ok = false;
        } else {
            goalnamelayout.setError("");
        }
        if ((goalamount.getText().toString().equals("")) || (goalamount.getText().toString().equals(".")) || (Double.valueOf(goalamount.getText().toString()) <= 0)) {
            goalamountlayout.setError("Enter Valid Amount ");
            ok = false;
        } else {
            goalamountlayout.setError("");
        }
        return ok;
    }

    private boolean checkdatachange(boolean wht) {
        boolean ok = false;
        if (wht) {
            if (!goalname.getText().toString().equals(editGoal.getLocal_goal_name())
                    || (goalamount.getText().toString().equals("" + editGoal.getLocal_current_amount()))
                    || (!selectedImage.equals(editGoal.getLocal_imagelink()))) {
                ok = true;
            }
        } else {
            if (!goalname.getText().toString().trim().equals("") || (!goalamount.getText().toString().equals(""))) {
                ok = true;
            }
        }
        return ok;
    }

    private ContentValues bundleup() {
        ContentValues dbvalues = new ContentValues();
        dbvalues.put(GOAL_NAME, String.valueOf(goalname.getText()));
        dbvalues.put(GOAL_COLLECTED_AMOUNT, 0);
        dbvalues.put(GOAL_TOTAL_AMOUNT, Double.valueOf(goalamount.getText().toString()));
        dbvalues.put(GOAL_IMAGE, selectedImage);
        return dbvalues;
    }

}
