package com.example.ravis.webs2.pojo;

/**
 * Created by ravis on 7/5/2017.
 */

public class goals_with_goalsof_particular_rule {
    public int goals_id = 0;

    public int getRules_id() {
        return rules_id;
    }

    public void setRules_id(int rules_id) {
        this.rules_id = rules_id;
    }

    public int rules_id=0;
    public String goal_name = "Create New Goal";
    public int total_amount = 0;
    public int current_amount = 0;
    public String imagelink = "";

    public int getGoals_id() {
        return goals_id;
    }

    public void setGoals_id(int goals_id) {
        this.goals_id = goals_id;
    }

    public String getGoal_name() {
        return goal_name;
    }

    public void setGoal_name(String goal_name) {
        this.goal_name = goal_name;
    }

    public int getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(int total_amount) {
        this.total_amount = total_amount;
    }

    public int getCurrent_amount() {
        return current_amount;
    }

    public void setCurrent_amount(int current_amount) {
        this.current_amount = current_amount;
    }

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }
}
