package com.example.ravis.webs2.goals;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.rules_and_accounts;
import com.example.ravis.webs2.rules.Rule_createnew;
import com.example.ravis.webs2.rules.helperfunctions;

import java.util.ArrayList;

public class Goal_chooseRule extends AppCompatActivity {

    TextView done;

    LinearLayout newrule;

    CardView activeRuleCard, pausedRuleCard;

    RecyclerView activerule, pausedrules;

    Goal_chooseRuleAdapter adapter, adapter1;

    RecyclerView.LayoutManager layoutManager, lm;

    ArrayList<Integer> preselected = new ArrayList<>();

    boolean isEdited = false;

    ArrayList<rules_and_accounts> pausedRulesData, activeRulesData;

    private static final String selectedRuleKey = "selectedrules";

    private static final String goalId = "goalid";

    private static final String isedited = "isedited";

    ArrayList<Integer> stListpausedrules,stListforactive_rules;

    int id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.goal_z_selectrules);

        stListforactive_rules=new ArrayList<>();
        stListpausedrules=new ArrayList<>();

        ImageView onback = (ImageView) findViewById(R.id.cback);
        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        done = (TextView) findViewById(R.id.done);

        Intent i = getIntent();
        preselected = i.getIntegerArrayListExtra(selectedRuleKey);
        isEdited = i.getBooleanExtra(isedited, false);
        id = i.getIntExtra(goalId, -1);

        newrule = (LinearLayout) findViewById(R.id.newrule);
        newrule.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent i = new Intent(Goal_chooseRule.this, Rule_createnew.class);
                helperfunctions.AfterNewRuleReurnToClass = Goal_chooseRule.class;
                startActivity(i);

            }
        });

        pausedRulesData = new ArrayList<>();
        activeRulesData = new ArrayList<>();

        getData();


        //for active rule
        if (activeRulesData.size() == 0) {
            activeRuleCard = (CardView) findViewById(R.id.cactivecard);
            activeRuleCard.setVisibility(View.GONE);

        } else {
            adapter = new Goal_chooseRuleAdapter(activeRulesData, this, preselected);
            activerule = (RecyclerView) findViewById(R.id.recyclerView);
            activerule.setNestedScrollingEnabled(false);
            layoutManager = new LinearLayoutManager(this);
            activerule.setLayoutManager(layoutManager);
            activerule.setAdapter(adapter);
        }
        //for paused rules
        if (pausedRulesData.size() == 0) {
            pausedRuleCard = (CardView) findViewById(R.id.cpausedcard);
            pausedRuleCard.setVisibility(View.GONE);
        } else {
            adapter1 = new Goal_chooseRuleAdapter(pausedRulesData, this, preselected);
            pausedrules = (RecyclerView) findViewById(R.id.recyclerView1);
            pausedrules.setNestedScrollingEnabled(false);
            pausedrules.setHasFixedSize(true);
            lm = new LinearLayoutManager(this);
            pausedrules.setLayoutManager(lm);
            pausedrules.setAdapter(adapter1);
        }

        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (adapter != null) {
                    stListforactive_rules = adapter.getselected_ruleid();
                }
                if (adapter1 != null) {
                    stListpausedrules = adapter1.getselected_ruleid();
                }
                stListforactive_rules.addAll(stListpausedrules);

                Intent i = new Intent();
                i.putIntegerArrayListExtra("selectedrules", stListforactive_rules);
                setResult(Activity.RESULT_OK, i);
                finish();
                stListforactive_rules.clear();
                stListpausedrules.clear();
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();

    }

    private void getData() {

        Cursor check = new SqlTransactions(Goal_chooseRule.this).activepausedrule(id);
        if (check.getCount() != 0) {
            while (check.moveToNext()) {
                if (check.getString(0) != null && check.getString(4) != null) {

                    rules_and_accounts obj = new rules_and_accounts();
                    obj.setId(Integer.parseInt(check.getString(0)));
                    obj.setRulename((check.getString(1)));
                    obj.setAmount(Integer.parseInt(check.getString(2)));
                    obj.setRuleimage(check.getString(3));
                    //obj.setFundedbank(check.getString(5).toString());
                    obj.setBankIcon(check.getString(6));
                    if (!isEdited) {
                        if (check.getString(5) != null) {
                            preselected.add(Integer.parseInt(check.getString(0)));
                        }
                    }
                    activeRulesData.add(obj);

                } else {

                    rules_and_accounts obj = new rules_and_accounts();
                    obj.setId(Integer.parseInt(check.getString(0)));
                    obj.setRulename((check.getString(1)));
                    obj.setAmount(Integer.parseInt(check.getString(2)));
                    obj.setRuleimage(check.getString(3));
                    //obj.setFundedbank(check.getString(5).toString());
                    obj.setBankIcon(check.getString(6));
                    pausedRulesData.add(obj);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}