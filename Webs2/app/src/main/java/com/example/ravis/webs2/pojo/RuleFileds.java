package com.example.ravis.webs2.pojo;

import java.util.List;

/**
 * Created by ravis on 7/1/2017.
 */

public class RuleFileds {
    public int ruleid = 0, saveamount = 0, savedamount = 0, duration = 0, savepercentage = 0;
    public String rulename = null, merchant = null, fundedbank = null, tiggeringbank = null,ruleimage="";
    public List<Integer> towardsgoalids = null;
    public boolean weekorder = false, ispaused = false;


    public int getRuleid() {
        return ruleid;
    }

    public void setRuleid(int ruleid) {
        this.ruleid = ruleid;
    }

    public int getSaveamount() {
        return saveamount;
    }

    public void setSaveamount(int saveamount) {
        this.saveamount = saveamount;
    }

    public int getSavedamount() {
        return savedamount;
    }

    public void setSavedamount(int savedamount) {
        this.savedamount = savedamount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getSavepercentage() {
        return savepercentage;
    }

    public void setSavepercentage(int savepercentage) {
        this.savepercentage = savepercentage;
    }

    public String getRuleimage() {
        return ruleimage;
    }

    public void setRuleimage(String ruleimage) {
        this.ruleimage = ruleimage;
    }

    public String getRulename() {
        return rulename;
    }

    public void setRulename(String rulename) {
        this.rulename = rulename;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getFundedbank() {
        return fundedbank;
    }

    public void setFundedbank(String fundedbank) {
        this.fundedbank = fundedbank;
    }

    public String getTiggeringbank() {
        return tiggeringbank;
    }

    public void setTiggeringbank(String tiggeringbank) {
        this.tiggeringbank = tiggeringbank;
    }

    public List<Integer> getTowardsgoalids() {
        return towardsgoalids;
    }

    public void setTowardsgoalids(List<Integer> towardsgoalids) {
        this.towardsgoalids = towardsgoalids;
    }

    public boolean isWeekorder() {
        return weekorder;
    }

    public void setWeekorder(boolean weekorder) {
        this.weekorder = weekorder;
    }

    public boolean ispaused() {
        return ispaused;
    }

    public void setIspaused(boolean ispaused) {
        this.ispaused = ispaused;
    }
}
