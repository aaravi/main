package com.example.ravis.webs2.goals;




import com.example.ravis.webs2.pojo.CustomCover;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Kshitij Mittal on 03-07-2017.
 */

public interface Goal_changeCoverService {

    @GET("index.php")
    Call<List<CustomCover>> getImageList(@Query("q") String search);
}
