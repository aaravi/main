package com.example.ravis.webs2.pojo;

import android.support.annotation.Nullable;

import com.example.ravis.webs2.pojo.RuleFileds;
import com.example.ravis.webs2.pojo.TowardsBankAccount;

import java.util.List;

/**
 * Created by aasthu on 03-Jul-17.
 */

public class rules_and_accounts
{
    private int id=0;
    private int amount=0;
    private int savedamount=0,goalid=0;

    public int getMappingid() {
        return mappingid;
    }

    public int getGoalid() {
        return goalid;
    }

    public void setGoalid(int goalid) {
        this.goalid = goalid;
    }

    public void setMappingid(int mappingid) {
        this.mappingid = mappingid;
    }

    private int mappingid=0;
    private String rulename=null;
    private String fundingbank=null;
    private String tiggeringbank=null;


    public String getRuleimage() {
        return ruleimage;
    }

    public void setRuleimage(String ruleimage) {
        this.ruleimage = ruleimage;
    }

    private String ruleimage=null;
    String bankIcon=null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getSavedamount() {
        return savedamount;
    }

    public void setSavedamount(int savedamount) {
        this.savedamount = savedamount;
    }

    public String getRulename() {
        return rulename;
    }

    public void setRulename(String rulename) {
        this.rulename = rulename;
    }

    public String getFundingbank() {
        return fundingbank;
    }

    public void setFundingbank(String fundingbank) {
        this.fundingbank = fundingbank;
    }

    public String getTiggeringbank() {
        return tiggeringbank;
    }

    public void setTiggeringbank(String tiggeringbank) {
        this.tiggeringbank = tiggeringbank;
    }

    public String getBankIcon() {
        return bankIcon;
    }


    public void setBankIcon(@Nullable String bankIcon) {
        this.bankIcon = bankIcon;
    }




}
