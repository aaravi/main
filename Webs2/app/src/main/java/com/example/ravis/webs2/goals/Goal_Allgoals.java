package com.example.ravis.webs2.goals;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.NewGoalPojo;


public class Goal_Allgoals extends AppCompatActivity {


    Context context;

    FloatingActionButton fabAddNew;
    CardView emptycard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goal_a_allgoals);

        fabAddNew = (FloatingActionButton) findViewById(R.id.firstpagefabadd);

        fabAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Goal_Allgoals.this, Goal_createNew.class);
                startActivity(in);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
        NewGoalPojo pojo = new NewGoalPojo(context);
        if (pojo.arr != null) {
            RecyclerView firstpageRview;
            Goal_AllgoalsAdapter firstpageRviewAdapter;
            RecyclerView.LayoutManager manager;
            NestedScrollView nestScroll;
            emptycard = (CardView) findViewById(R.id.emptycard);
            emptycard.setVisibility(View.INVISIBLE);
            firstpageRview = (RecyclerView) findViewById(R.id.firstpageRview);
            manager = new LinearLayoutManager(context);
            firstpageRview.setNestedScrollingEnabled(false);
            firstpageRview.setLayoutManager(manager);
            firstpageRviewAdapter = new Goal_AllgoalsAdapter(context, pojo);
            firstpageRview.setAdapter(firstpageRviewAdapter);

            nestScroll = (NestedScrollView) findViewById(R.id.nestscroll);
            //custom code to hide fab while scrolling ----------------------------------------------
            nestScroll.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {
                        fabAddNew.hide();
                    } else {
                        fabAddNew.show();
                    }
                }
            });


        } else {
            emptycard = (CardView) findViewById(R.id.emptycard);
            emptycard.setVisibility(View.VISIBLE);
            emptycard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, Goal_createNew.class);
                    startActivity(in);
                }
            });
        }


    }
}
