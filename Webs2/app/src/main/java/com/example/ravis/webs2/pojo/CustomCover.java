
package com.example.ravis.webs2.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomCover {

    @SerializedName("thumbnailLink")
    @Expose
    private String thumbnailLink;
    @SerializedName("link")
    @Expose
    private String link;



    public String getThumbnailLink() {return thumbnailLink;}
    public void setThumbnailLink(String thumbnailLink) {this.thumbnailLink = thumbnailLink;}

    public String getLink() {return link;}
    public void setLink(String link) {this.link = link;}

}