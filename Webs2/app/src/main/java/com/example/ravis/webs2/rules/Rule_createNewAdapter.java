package com.example.ravis.webs2.rules;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.newRulePojo;

import java.util.ArrayList;

/**
 * Created by ravis on 6/7/2017.
 * create rule card adapter
 */

public class Rule_createNewAdapter extends RecyclerView.Adapter<Rule_createNewAdapter.viewHolder> {
    static Context context;
    ArrayList<newRulePojo> list=new ArrayList<>();

    public Rule_createNewAdapter(Context context) {
        this.context = context;
        list=newRulePojo.DummyRules();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView img;
        public TextView title;
        public TextView desc;
        public String color;
        private static final String typeKey = "type";

        public viewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.maincardview);
            img = (ImageView) itemView.findViewById(R.id.cardviewimage);
            title = (TextView) itemView.findViewById(R.id.CardTitle);
            desc = (TextView) itemView.findViewById(R.id.CardDescription);
            color = "";


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    switch (getAdapterPosition()) {
                        case 0: {
                            Intent in = new Intent(context, Rule_roundup.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }

                        case 1: {
                            Intent in = new Intent(context, Rule_spendless.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }
                        case 2: {
                            Intent in = new Intent(context, Rule_guiltypleasure.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }
                        case 3: {
                            Intent in = new Intent(context, Rule_setforget.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }
                        case 4: {
                            Intent in = new Intent(context, Rule_52week.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }
                        case 5: {
                            Intent in = new Intent(context, Rule_freelancer.class);
                            in.putExtra(typeKey, true);
                            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(in);
                            break;
                        }
                        default:
                            Toast.makeText(context, "Sorry Rule not recognized", Toast.LENGTH_SHORT).show();
                    }


                }

            });
        }
    }

    @Override
    public Rule_createNewAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rule_a_creatnewcard, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {

        holder.desc.setText(list.get(position).getDesc());
        holder.title.setText(list.get(position).getTitle());
        holder.cardView.setCardBackgroundColor(Color.parseColor(list.get(position).getColor()));
        holder.img.setImageResource(list.get(position).getImg());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
