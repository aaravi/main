package com.example.ravis.webs2.goals;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.RuleFileds;
import com.example.ravis.webs2.rules.Rule_freelancer;
import com.example.ravis.webs2.rules.Rule_guiltypleasure;
import com.example.ravis.webs2.rules.Rule_roundup;
import com.example.ravis.webs2.rules.Rule_setforget;
import com.example.ravis.webs2.rules.Rule_spendless;
import com.example.ravis.webs2.rules.Rule_52week;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Kshitij Mittal on 07-07-2017.
 */

public class SelectedRulesAdapter extends RecyclerView.Adapter<SelectedRulesAdapter.viewHolder> {
    private ArrayList<RuleFileds> rulesdata = new ArrayList<>();

    Context context;
    private static final String typeKey = "type";
    private static final String ruliId = "ruleid";

    SelectedRulesAdapter(Context context, ArrayList<RuleFileds> rulesdata) {
        this.rulesdata = rulesdata;
        this.context = context;
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        CardView selectedRule;
        ImageView ruleimage;
        TextView rulename;

        public viewHolder(View itemView) {
            super(itemView);
            selectedRule = (CardView) itemView.findViewById(R.id.selectedRule);
            ruleimage = (ImageView) itemView.findViewById(R.id.ruleimage);
            rulename = (TextView) itemView.findViewById(R.id.rulename);
            selectedRule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (rulesdata.get(getAdapterPosition()).getRulename()) {
                        case "Round Up Rule": {
                            Intent in = new Intent(context, Rule_roundup.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }

                        case "Spend Less Rule": {
                            Intent in = new Intent(context, Rule_spendless.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }
                        case "Guilty Pleasure Rule": {
                            Intent in = new Intent(context, Rule_guiltypleasure.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }
                        case "Set & Forget Rule": {
                            Intent in = new Intent(context, Rule_setforget.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }
                        case "52 Week Rule": {
                            Intent in = new Intent(context, Rule_52week.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }
                        case "Freelancer Rule": {
                            Intent in = new Intent(context, Rule_freelancer.class);
                            in.putExtra(typeKey, false);
                            in.putExtra(ruliId, rulesdata.get(getAdapterPosition()).getRuleid());
                            context.startActivity(in);
                            break;
                        }
                        default:
                            Toast.makeText(context, "Sorry Rule not recognized", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.goal_b_goaldetails_rulescard, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(viewHolder holder, int position) {
        Picasso.with(context)
                .load(rulesdata.get(position).getRuleimage())
                .placeholder(R.drawable.ic_imageholder)
                .error(R.drawable.ic_errorimage)
                .into(holder.ruleimage);
        holder.rulename.setText(rulesdata.get(position).getRulename());
    }

    @Override
    public int getItemCount() {
        return rulesdata.size();
    }


}
