package com.example.ravis.webs2.DatabaseHandle;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.ravis.webs2.pojo.Goals;
import com.example.ravis.webs2.pojo.RuleFileds;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;
import com.example.ravis.webs2.pojo.rules_and_accounts;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.COLUMN_ACCOUNTS_icon_url;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.COLUMN_ACCOUNTS_providerName;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.COLUMN_ACCOUNTS_yodlee_id;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_COLLECTED_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_ID;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.GOAL_TOTAL_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_ID;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.TABLE_ACCOUNTS;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.TABLE_GOALS;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.TABLE_MAPPING;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.TABLE_RULES;


public class SqlTransactions {

    public static final String QUERY_GET_RULES_FOR_A_GOAL = "select distinct " + TABLE_RULES + "." + RULE_NAME + "," +

            TABLE_MAPPING + "." + GOAL_ID + "," +

            TABLE_RULES + "." + RULE_ID + "," +

            TABLE_RULES + "." + RULE_IMAGE + "," +

            TABLE_RULES + "." + RULE_SAVE_AMOUNT +

            " from " + TABLE_RULES + "," + TABLE_GOALS + "," + TABLE_MAPPING +

            " where " + TABLE_MAPPING + "." + RULE_ID + "=" + TABLE_RULES + "." + RULE_ID + " and " +

            TABLE_MAPPING + "." + GOAL_ID + "=";

    public static final String QUERY_GET_GOALS_FOR_A_RULE = "SELECT " + TABLE_GOALS + "." + GOAL_ID + "," + TABLE_GOALS + "." + GOAL_NAME + ","

            + TABLE_GOALS + "." + GOAL_COLLECTED_AMOUNT + "," + TABLE_GOALS + "." + GOAL_TOTAL_AMOUNT + "," + TABLE_GOALS + "." + GOAL_IMAGE + ","

            + TABLE_MAPPING + "." + RULE_ID

            + " FROM " + TABLE_GOALS

            + " LEFT JOIN " + TABLE_MAPPING + " ON " + TABLE_GOALS + "." + GOAL_ID + " = " + TABLE_MAPPING + "." + GOAL_ID + " AND "

            + TABLE_MAPPING + "." + RULE_ID + " = ";

    public static final String RULE_DATA = "select * from " + TABLE_RULES + " where " + RULE_ID + "=";

    public static final String LATEST_RULE = "select max(" + RULE_ID + ") from " + TABLE_RULES;

    public static final String RULE_DATA_WITH_BANK_NAME = "select rules.ruleid,rules.rulename," +
            "rules.saveamount,rules.merchant" +
            ",rules.savepercentage,rules.weekorder" +
            ",rules.duration" +
            ",rules.savedamount,rules.ruleimages," +
            "(select accounts.providerName from rules,accounts" +
            "where rules.ruleid=5 and rules.triggeringbank=accounts.yodlee_id)  " +
            "as providername_for_triggeringbank" +
            " ,(select accounts.providerName" +
            " from rules,accounts" +
            "where rules.ruleid=5 and " +
            "rules.fundedbank=accounts.yodlee_id) " +
            "as providername_for_fundedbank from rules" +
            "left join accounts on (rules.triggeringbank = accounts.yodlee_id " +
            "or rules.fundedbank=accounts.yodlee_id) " +
            "where rules.ruleid=5;";


    ArrayList<Goals> user_goals_Array = new ArrayList<>();

    ArrayList<RuleFileds> user_rules_Array = new ArrayList<>();

    ArrayList<rules_and_accounts> rules_and_accounts = new ArrayList<>();

    ArrayList<TowardsBankAccount> temp_bankAccountArrayList = new ArrayList<>();

    ArrayList<goals_with_goalsof_particular_rule> goals_with_goalsof_particular_rules = new ArrayList<>();

    RuleFileds ruleFileds = new RuleFileds();

    SQLiteOpenHelper DbHelper;

    SQLiteDatabase db;

    Context context;

    long maxruleid = 0;

    public SqlTransactions(Context context) {

        this.context = context;

        DbHelper = new DatabaseCreator(context);

        db = DbHelper.getWritableDatabase();
    }

    public void closeDatabase() {
        //  DbHelper.close();
        db.close();

    }

    /*public static final String DATABASE_NAME = "capgoalrules.db";

    //table name
    public static final String TABLE_GOALS = "goals";
    public static final String TABLE_RULES = "rules";
    public static final String TABLE_MAPPING = "mapping";
    public static final String TABLE_ACCOUNTS = "accounts";

    //Goals column names
    public static final String GOAL_ID = "goalid"; //mapping column name
    public static final String GOAL_NAME = "goalname";
    public static final String GOAL_COLLECTED_AMOUNT = "collectedamount";
    public static final String GOAL_TOTAL_AMOUNT = "totalamount";
    public static final String GOAL_IMAGE = "images";
    public static final String CREATE_GOAL_TABLE =
            "CREATE TABLE " + TABLE_GOALS + "( "
                    + GOAL_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                    + GOAL_NAME + " TEXT, "
                    + GOAL_COLLECTED_AMOUNT + " REAL, "
                    + GOAL_TOTAL_AMOUNT + " REAL, "
                    + GOAL_IMAGE + " TEXT ); ";


    public SqlTransactions(Context context) {
        super(context, DatabaseCreator.DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseCreator.TABLE_GOALS);
        onCreate(db);

    }
*/
//get rules data with bank name from database for a particular rule id--------------------------------------------------------

    public Cursor getRuledataWithBankName(int ruleid) {

        String getRules = "select  " + TABLE_RULES + ".*," + TABLE_ACCOUNTS + "." +

                COLUMN_ACCOUNTS_yodlee_id + " as yodlee_id_for_trigerring_bank, " + TABLE_ACCOUNTS + "." +

                COLUMN_ACCOUNTS_providerName + " as providername_for_trigerring_bank,(" +

                " select " + TABLE_ACCOUNTS + "." +

                COLUMN_ACCOUNTS_providerName + " from " + TABLE_RULES + "," + TABLE_ACCOUNTS + " where " + TABLE_RULES + "." + RULE_ID + " = " + ruleid + " and "

                + TABLE_RULES + "." + RULE_FUNDED_BANK + " = " + TABLE_ACCOUNTS + "." + COLUMN_ACCOUNTS_yodlee_id

                + ") as providername_for_trigerring_bank , " + "(" +

                " select " + TABLE_ACCOUNTS + "." +

                COLUMN_ACCOUNTS_yodlee_id + " from " + TABLE_RULES + "," + TABLE_ACCOUNTS + " where " + TABLE_RULES + "." + RULE_ID + " = " + ruleid + " and "

                + TABLE_RULES + "." + RULE_FUNDED_BANK + " = " + TABLE_ACCOUNTS + "." + COLUMN_ACCOUNTS_yodlee_id

                + ") as yodlee_id_for_funded_bank from " + TABLE_RULES + " left join " + TABLE_ACCOUNTS + " on "

                + TABLE_RULES + "." + RULE_TRIGERRING_BANK + " = " + TABLE_ACCOUNTS + "." + COLUMN_ACCOUNTS_yodlee_id + " where " +

                TABLE_RULES + "." + RULE_ID + " = " + ruleid;
        Cursor cursor = db.rawQuery(getRules, null);

        return cursor;

    }


    //get Goals table from database-----------------------------------------------------------------------------------------------
    public Cursor getGoalsFromDatabase() {


        Cursor cursor = db.rawQuery("select * from " + DatabaseCreator.TABLE_GOALS, null);

        return cursor;
    }

    //store goals into arraylist----------------------------------------------------------------------------------------------------

    public ArrayList<Goals> arraylistallGoalsfromDb() {

        Cursor res = getGoalsFromDatabase();

        if (res.getCount() == 0)

        {

            return null;

        }

        while (res.moveToNext()) {

            Goals copy = new Goals();

            copy.setLocal_goals_id(Integer.parseInt(res.getString(0)));

            copy.setLocal_goal_name(res.getString(1));

            copy.setLocal_current_amount(Integer.parseInt(res.getString(2)));

            copy.setLocal_total_amount(Integer.parseInt(res.getString(3)));

            copy.setLocal_imagelink(res.getString(4));

            user_goals_Array.add(copy);
        }

        return user_goals_Array;
    }

    //get Bank name,id and image from database ---------------------------------------------------------------------------------------
    public Cursor getBankData() {

        Cursor cursor = db.rawQuery("select yodlee_id,providerName,icon_url from " + DatabaseCreator.TABLE_ACCOUNTS, null);

        return cursor;
    }

    //store banks into arraylist-------------------------------------------------------------------------------------------------------
    public ArrayList<TowardsBankAccount> fundingBankAccount() {

        Cursor res = getBankData();

        if (res.getCount() == 0)

        {

            return null;

        }

        while (res.moveToNext())

        {
            TowardsBankAccount temp = new TowardsBankAccount();

            temp.setYodlee_id(Integer.parseInt(res.getString(0)));

            temp.setBankName(res.getString(1));

            temp.setBankIcon(res.getString(2));

            temp_bankAccountArrayList.add(temp);
        }

        return temp_bankAccountArrayList;

    }


    //get All rules for a particular goals--------------------------------------------------------------------------------

    public Cursor getRulesInCursor()

    {

        Cursor cursor = db.rawQuery("select * from " + DatabaseCreator.TABLE_RULES + " where ", null);

        return cursor;

    }


    // update values for a given rule------------------------------------------------------------------------------------------

    public long updateRule(ContentValues value, int Ruleid)

    {

        long result = db.update(TABLE_RULES, value, RULE_ID + "=" + Ruleid, null);

        return result;

    }


    //while performing update operation for a particular rule certain operation like updating values in rules table,-----------------------------------------------------
    //deleting old mappings and updating new mappings had to be performed which is all done by this :
    public long updateRuleWithMappings(ContentValues dbvalues, int ruleid, ArrayList<Integer> finalSelectedGoals) {

        long result = 0;

        ContentValues dbValues = dbvalues;

        result = new SqlTransactions(context).updateRule(dbValues, ruleid);

        Toast.makeText(context, (result != -1 ? "Rule updated" : "Error updating rule"), Toast.LENGTH_SHORT).show();

        new SqlTransactions(context).deleteAllGoalsForARuleFromMapping(ruleid);

        Toast.makeText(context, (result != -1 ? "Goals deleted from mapping" : "Error deleting mapped goals"), Toast.LENGTH_SHORT).show();

        new SqlTransactions(context).insertmappingupdaterule(finalSelectedGoals, ruleid);

        Toast.makeText(context, result != -1 ? "Mappings Updated" : "Error updataing mappings", Toast.LENGTH_SHORT).show();
        return result;
    }


    //delete all goals for a particular rule in table mapping------------------------------------------------------------------

    public long deleteAllGoalsForARuleFromMapping(int RuleidtoDeleteFromMapping)

    {

        long result = db.delete(TABLE_MAPPING, RULE_ID + "=" + RuleidtoDeleteFromMapping, null);

        return result;
    }


    //insert into mapping new Goals for a Rule

    public long insertIntoMappingAllGoalsForARule(int Ruleid, ContentValues values, ArrayList<Integer> GoalsToinsert) {

        long result = 0;

        for (int browse : GoalsToinsert) {
            result = db.insert(TABLE_MAPPING, null, values);

            //if value is not inserted result will be -1 resulting in termination of loop

            if (result == -1)
                break;

        }

        return result;

    }

    // active or paused rules-----------------------------------------------------------------------------------------------------------
    public Cursor activepausedrule(int goalid) {

        final String get_rules_data_with_goaland_bank = "select " + TABLE_RULES + "." + RULE_ID + ","

                + TABLE_RULES + "." + RULE_NAME + "," + TABLE_RULES + "." + RULE_SAVE_AMOUNT + "," + TABLE_RULES + "." + RULE_IMAGE + ","

                + "( select " + TABLE_MAPPING + "." + RULE_ID + " from " + TABLE_MAPPING + " where " + TABLE_MAPPING + "." + RULE_ID + "=" + TABLE_RULES + "." + RULE_ID

                + ") as mappingruleid, ( select " + TABLE_MAPPING + "." + GOAL_ID + " from " + TABLE_MAPPING + " where " + TABLE_MAPPING + "." +

                RULE_ID + "=" + TABLE_RULES + "." + RULE_ID + " and "

                + TABLE_MAPPING + "." + GOAL_ID + "=" + goalid + " ) as mappinggoalid ," + TABLE_ACCOUNTS + "." + COLUMN_ACCOUNTS_icon_url + " from "

                + TABLE_RULES + " left join " + TABLE_ACCOUNTS + " on "

                + TABLE_RULES + "." + RULE_FUNDED_BANK + " =  " + TABLE_ACCOUNTS + "." + COLUMN_ACCOUNTS_yodlee_id;

        Cursor res = db.rawQuery(get_rules_data_with_goaland_bank, null);

        return res;
    }

    public ArrayList<rules_and_accounts> activepaused() {

        Cursor check = activepausedrule(1);

        if (check.getCount() == 0) {

            return null;

        }
        while (check.moveToNext()) {

            rules_and_accounts obj = new rules_and_accounts();

            obj.setRulename((check.getString(2)).toString());

            obj.setAmount(Integer.parseInt(check.getString(3)));

            obj.setRuleimage(check.getString(4).toString());

            obj.setBankIcon(check.getString(5).toString());

            rules_and_accounts.add(obj);

        }

        return rules_and_accounts;

    }

    public Cursor getRuleDetails(int ruleid) {

        Cursor c = db.rawQuery(RULE_DATA + ruleid, null);

        return c;

    }


    //GET ALL GOALS FOR A PARTICULAR RULE----------------------------------------------------------------------------

    public ArrayList<goals_with_goalsof_particular_rule> getallgoalsforrule(int particular_rule_id) {

        Cursor c = db.rawQuery(QUERY_GET_GOALS_FOR_A_RULE + particular_rule_id, null);
        final String QUERY_GET_GOALS_FOR_A_RULE2 = "SELECT " + TABLE_GOALS + "." + GOAL_ID + "," + TABLE_GOALS + "." + GOAL_NAME + ","

                + TABLE_GOALS + "." + GOAL_COLLECTED_AMOUNT + "," + TABLE_GOALS + "." + GOAL_TOTAL_AMOUNT + "," + TABLE_GOALS + "." + GOAL_IMAGE + ","

                + TABLE_MAPPING + "." + RULE_ID

                + " FROM " + TABLE_GOALS

                + " LEFT JOIN " + TABLE_MAPPING + " ON " + TABLE_GOALS + "." + GOAL_ID + " = " + TABLE_MAPPING + "." + GOAL_ID + " AND "

                + TABLE_MAPPING + "." + RULE_ID + " = ";
        if (c.getCount() == 0)

        {

            return null;

        } else {
            while (c.moveToNext())

            {
                goals_with_goalsof_particular_rule temp = new goals_with_goalsof_particular_rule();

                temp.setGoals_id(Integer.parseInt(c.getString(0)));

                temp.setGoal_name(c.getString(1));

                temp.setCurrent_amount(Integer.parseInt(c.getString(2)));

                temp.setTotal_amount(Integer.parseInt(c.getString(3)));

                temp.setImagelink(c.getString(4));

                temp.setRules_id((Integer.parseInt((c.getString(5) == null ? "0" : c.getString(5)))));

                goals_with_goalsof_particular_rules.add(temp);

            }

            return goals_with_goalsof_particular_rules;

        }
    }
    //get All rules for a particular goal id "particular_goal_id"-----------------------------------------------------------------

    public ArrayList<RuleFileds> getallRulesForAGoal(int particular_goal_id)

    {

        Cursor c = db.rawQuery(QUERY_GET_RULES_FOR_A_GOAL + particular_goal_id, null);

        if (c.getCount() == 0)

        {

            return null;

        }

        while (c.moveToNext())

        {

            RuleFileds temp = new RuleFileds();

            temp.setRulename(c.getString(0));

            temp.setRuleid(Integer.parseInt(c.getString(2)));

            temp.setRuleimage(c.getString(3));

            temp.setSaveamount(Integer.parseInt(c.getString(4)));

            user_rules_Array.add(temp);

        }

        return user_rules_Array;

    }


    /**
     * -------------------------------------------------------------------------------------------------------------------------
     * INSERT INTO TABLE function for Rules and Goals
     *
     * @param values brings corresponding data
     *               to insert into TABLE
     */

    public long insertRule(ContentValues values) {

        maxruleid = db.insert(TABLE_RULES, null, values);

        return maxruleid;

    }

    //insert into mapping for create new rule-----------------------------------------------------------------------------------------
    //max function is used here to insert these goals(iNteger arraylist)  for latest inserted rule
    public void insertmappingcreateRule(ArrayList<Integer> goalid) {

        if (goalid != null) {
            for (int browse : goalid) {
//            ContentValues contentValues=new ContentValues();

                String insertintomapping = "insert into " + TABLE_MAPPING + " values(" + browse + ",(select max(" + RULE_ID + ") from " + TABLE_RULES + "));";

                db.execSQL(insertintomapping);

            }

            db.close();
        }

    }


    //insert into mapping for update rule---------------------------------------------------------------------------------------
    //goalid=arraylist of goals for particular ruleid (2nd argument) to be inserted into mapping table
    public void insertmappingupdaterule(ArrayList<Integer> goalid, int ruleid) {

        long result;

        if (goalid != null)
            for (int browse : goalid) {
                ContentValues contentValues = new ContentValues();

                contentValues.put(RULE_ID, ruleid);

                contentValues.put(GOAL_ID, browse);

                result = db.insert(TABLE_MAPPING, null, contentValues);

                Toast.makeText(context, result + "", Toast.LENGTH_SHORT).show();
            }

        db.close();

    }

    //delete rule from rules table with provided rule id,
    // mappings will automaticaly deleted because of foreign key ruleid in mapping for
    // primary key in rules table
    public long deleteRule(int ruleid) {

        long result = db.delete(TABLE_RULES, RULE_ID + "=" + ruleid, null);
        return result;
    }

    public void insertGoal(ContentValues values,ArrayList<Integer> ruleids) {
        long maxgoalid = db.insert(TABLE_GOALS, null, values);

        if (ruleids.size() != 0) {
            for (int browse : ruleids) {
                String insertintomapping = "INSERT INTO " + TABLE_MAPPING
                        + "(" + GOAL_ID + "," + RULE_ID + ") VALUES(" + maxgoalid + "," + browse + ");";
                db.execSQL(insertintomapping);
            }
        }
    }

    public Goals goalToedit(int id) {
        Goals editgoal = new Goals();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_GOALS + " WHERE " + GOAL_ID + " = " + id + ";", null);
        if (c.moveToFirst()) {
            editgoal.setLocal_goals_id(Integer.parseInt(c.getString(0)));
            editgoal.setLocal_goal_name(c.getString(1));
            editgoal.setLocal_current_amount(Integer.parseInt(c.getString(2)));
            editgoal.setLocal_total_amount(Integer.parseInt(c.getString(3)));
            editgoal.setLocal_imagelink(c.getString(4));
            c.close();
        }

        return editgoal;
    }


    public void updateGoal(int id, ContentValues update, ArrayList<Integer> ruleid,boolean change) {
        db.update(TABLE_GOALS, update, GOAL_ID + " = " + id + ";", null);
        if (change) {
            deleteMappingWrtGoal(id);
            if (ruleid.size() != 0) {
                for (int browse : ruleid) {
                    String insertintomapping = "INSERT INTO " + TABLE_MAPPING
                            + "(" + GOAL_ID + "," + RULE_ID + ") VALUES(" + id + "," + browse + ");";
                    db.execSQL(insertintomapping);

                }
            }
        }
        db.close();
    }

    /**
     * DELETE FROM TABLE function for Rules, Goals and mapping
     *
     * @param id - from where data is to be deleted
     */

    public void deleteGoal(int id) {
        db.delete(TABLE_GOALS, GOAL_ID + " = " + id, null);
        db.close();
    }

    //delete from mapping table with respect to goal id
    public void deleteMappingWrtGoal(int id) {
        db.delete(TABLE_MAPPING, GOAL_ID + " = " + id, null);
    }
}
