package com.example.ravis.webs2.goals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.Goals;
import com.example.ravis.webs2.pojo.RuleFileds;
import com.example.ravis.webs2.rules.Rule_createnew;
import com.example.ravis.webs2.rules.helperfunctions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Goal_details extends AppCompatActivity {

    ImageView alreadycreatedgoals_imageview, editgoal, onback;
    TextView alreadycreatedgoals_goaltitle, goal_collected_amount, goal_total_amount;
    RecyclerView ruleschoosen;
    ProgressBar progress;
    LinearLayout alreadycreatedgoals_newrule;
    ArrayList<RuleFileds> arr = new ArrayList<>();
    SelectedRulesAdapter adapter;
    private static final String goalId = "goalid";
    int id = -1;
    Goals goal;
    SqlTransactions sqt;
    private static final String ReturnTo_Value = "Goal_details";
    private static final String ReturnTo_Key = "ReturnTo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.goal_b_goaldetails);

        // id = getIntent().getIntExtra(goalId, -1);    rather than getting id from intent value is retrieved from static variable using helperGoalPosition


        init();

        editgoal = (ImageView) findViewById(R.id.edit);
        editgoal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent in = new Intent(Goal_details.this, Goal_createEdit.class);
                in.putExtra(getString(R.string.intent), getString(R.string.edit));
                in.putExtra(goalId, goal.getLocal_goals_id());
                startActivity(in);
            }
        });


        alreadycreatedgoals_newrule.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //helperfunctions.helperposition = position_in_pojo;
                Intent in = new Intent(Goal_details.this, Rule_createnew.class);
                helperfunctions.AfterNewRuleReurnToClass = Goal_details.class;
                helperfunctions.helperGoalPosition=goal.getLocal_goals_id();
                startActivity(in);
            }
        });

        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
        preset();
        loadRecyclerView();
    }

    private void init() {

        goal_collected_amount = (TextView) findViewById(R.id.goal_collected_amount);
        goal_total_amount = (TextView) findViewById(R.id.goal_total_amount);
        progress = (ProgressBar) findViewById(R.id.saveProgress);
        alreadycreatedgoals_newrule = (LinearLayout) findViewById(R.id.alreadycreatedgoals_newrule);
        alreadycreatedgoals_goaltitle = (TextView) findViewById(R.id.alreadycreatedgoals_goaltitle);
        alreadycreatedgoals_imageview = (ImageView) findViewById(R.id.alreadycreatedgoals_imageview);
        ruleschoosen = (RecyclerView) findViewById(R.id.rulesChoosen);
        onback = (ImageView) findViewById(R.id.back);
    }

    private void preset() {
        id = helperfunctions.helperGoalPosition;
        sqt = new SqlTransactions(this);
        goal = sqt.goalToedit(id);

        alreadycreatedgoals_goaltitle.setText(goal.getLocal_goal_name());
        String a = goal.getLocal_goal_name();
        goal_collected_amount.setText(String.valueOf(goal.getLocal_current_amount()));
        goal_total_amount.setText(String.valueOf(goal.getLocal_total_amount()));
        Picasso.with(Goal_details.this)
                .load(goal.getLocal_imagelink())
                .placeholder(R.drawable.ic_imageholder)
                .error(R.drawable.ic_errorimage)
                .into(alreadycreatedgoals_imageview);
        progress.setMax(goal.getLocal_total_amount());
        progress.setProgress(goal.getLocal_current_amount());
    }

    private void loadRecyclerView() {
        arr = new SqlTransactions(this).getallRulesForAGoal(id);
        if (arr != null) {
            ruleschoosen.setVisibility(View.VISIBLE);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            ruleschoosen.setLayoutManager(mLayoutManager);
            adapter = new SelectedRulesAdapter(this, arr);
            ruleschoosen.setAdapter(adapter);
        }
        else
            ruleschoosen.setVisibility(View.INVISIBLE);

    }


}