package com.example.ravis.webs2.rules;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListView;
import android.widget.TextView;

import com.example.ravis.webs2.R;

import java.util.List;

/**
 * Created by aasthu on 21-Jun-17.
 */

public class classforsingleandmultiselect {

    Context context;

    TextView editText, ordervalue, weekvalue;

    String selected = new String();

    int[] images;

    float amountttt;

    boolean[] aa = {false, true, false};

    String[] a, order_of_deduction;

    String title;

    AlertDialog.Builder builder;

    Dialog dialog;

    int position;

    List<String> colorsList;

    int multiplier = 10;

    static String[] order_for_money_deduction = new String[2];

    static final String[] valuefor_Order_Deduction = new String[2];

    public classforsingleandmultiselect(Context context) {

        this.context = context;
    }

    public classforsingleandmultiselect(Context context, TextView editText, String a[], String title) {

        builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialog = new Dialog(context);
        this.context = context;
        this.editText = editText;
        this.title = title;
        this.a = a;
    }


    public classforsingleandmultiselect(Context context, TextView editText, String a[], String title, TextView Ordervalue, String[] orderof_moneydeduction) {

        builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialog = new Dialog(context);
        this.context = context;
        this.editText = editText;
        this.title = title;
        this.a = a;
        this.ordervalue = Ordervalue;
        this.order_of_deduction = orderof_moneydeduction;
    }

    public classforsingleandmultiselect(Context context, TextView editText, String a[], String title, TextView weekvalue) {

        builder = new AlertDialog.Builder(context, R.style.MyDialogTheme);
        dialog = new Dialog(context);
        this.context = context;
        this.editText = editText;
        this.title = title;
        this.a = a;
        this.weekvalue = weekvalue;
        this.position = position;
    }


//    public classforsingleandmultiselect(Context context,TextView editText ,String a[],int[] images) {
//        builder=new AlertDialog.Builder(context,R.style.MyDialogTheme);
//        this.context = context;
//        this.editText=editText;
//        this.images=images;
//        this.a=a;
//    }


    String singlechoiceselection() {

        String s = editText.getText().toString();
        int selectedindex = 0;
        for (int i = 0; i < a.length; i++) {
            if (s.equals(a[i])) {
                selectedindex = i;
            }
        }

        builder.setTitle(title);
        builder.setSingleChoiceItems(a, selectedindex, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        }).setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView lw = ((AlertDialog) dialog).getListView();
                Object checkedItem = lw.getAdapter().getItem(lw.getCheckedItemPosition());
                selected = (checkedItem.toString());
                editText.setText(selected);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create();
        builder.show();
        s = selected;
        return s;
    }
//    void multiselectionchoice()
//    {
//        //ListAdapter adapter=new ArrayAdapterWithIcon(context,a,images);
//        builder.setTitle("Hello World");
//        builder.setMultiChoiceItems(a, aa, new DialogInterface.OnMultiChoiceClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//                aa[which]=isChecked;
//            }
//        });
//        builder.setPositiveButton("ok",new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                colorsList = Arrays.asList(a);
//                editText.setText("Your preferred colors..... \n");
//                String colors = "";
//                for (int i = 0; i < aa.length; i++) {
//                    boolean checked = aa[i];
//                    if (checked) {
//                        colors = colors + colorsList.get(i) ;
//                        if (i != aa.length - 1) {
//                            colors = colors + ", ";
//                        }
//                    }
//                }
//                editText.setText(editText.getText() + colors);
//                dialog.cancel();
//
//
//            }}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//            }
//        }).create();
//        builder.show();
//
//    }

    //for week value and setting the amount in order of deduction============================
    String[] singlechoiceselection_forweek() {


        String week_text = editText.getText().toString();
        String checkfor_regular_reversed = ordervalue.getText().toString();
        int selectedindex_forWeekValue = 0, selectedindex_for_order = 0;
        for (int i = 0; i < a.length; i++) {
            if (week_text.equals(a[i])) {
                selectedindex_forWeekValue = i;
            }
        }
        for (int i = 0; i < 2; i++) {
            if (checkfor_regular_reversed.equalsIgnoreCase(order_of_deduction[i])) {
                selectedindex_for_order = i;
            }
        }

        builder.setTitle(title);
        final int finalSelectedindex_for_order = selectedindex_for_order;
        builder.setSingleChoiceItems(a, selectedindex_forWeekValue, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        }).setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                int amountt = 0;
                ListView lw = ((AlertDialog) dialog).getListView();
                Object checkedItem = lw.getAdapter().getItem(lw.getCheckedItemPosition());
                selected = (checkedItem.toString());
                String weekval = selected.replaceAll(".*?[a-zA-z-₹$]", "").replace("\"", "");
                amountt = Integer.parseInt(weekval.replace("\"", ""));
                valuefor_Order_Deduction[0] = "regular (Start with ₹" + amountt + ")";
                valuefor_Order_Deduction[1] =
                        "reversed (start with ₹" + ((((52 * multiplier) - amountt) + multiplier)) +
                                ")";
                editText.setText(selected);
                if (finalSelectedindex_for_order == 0)
                    ordervalue.setText(valuefor_Order_Deduction[0]);
                else
                    ordervalue.setText(valuefor_Order_Deduction[1]);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create();
        builder.show();
        return valuefor_Order_Deduction;
    }

    // for deciding the order of deduction and setting the values in week array
    void singlechoiceselection_fororder() {

        String orderDeduction = editText.getText().toString();
        final StringBuffer extractedvalue = new StringBuffer();
        char week[] = weekvalue.getText().toString().toCharArray();
        for (int i = 0; i < week.length; i++) {
            while (Character.isDigit(week[i])) {
                extractedvalue.append(week[i]);
                i++;
            }
            if (week[i] == '-') break;
        }
        int selectedindex = 0;
        for (int i = 0; i < a.length; i++) {
            if (orderDeduction.equals(a[i])) {
                selectedindex = i;
            }
        }
        builder.setTitle(title);
        builder.setSingleChoiceItems(a, selectedindex, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        }).setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                ListView lw = ((AlertDialog) dialog).getListView();

                Object checkedItem = lw.getAdapter().getItem(lw.getCheckedItemPosition());

                selected = (checkedItem.toString());

                editText.setText(selected);

                String weekval = selected.toString().replaceAll("[A-Za-z]|[$]|[\\s]|[₹]|[()]", "");

                int amount = Integer.parseInt(weekval);

                if (selected.toLowerCase().contains("reversed"))

                    weekvalue.setText("week " + (extractedvalue) + "-₹" + amount);

                else

                    weekvalue.setText("week " + (extractedvalue) + "-₹" + amount);

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).create();
        builder.show();
    }


}


