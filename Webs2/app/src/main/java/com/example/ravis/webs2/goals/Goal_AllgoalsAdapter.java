package com.example.ravis.webs2.goals;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.NewGoalPojo;
import com.example.ravis.webs2.rules.helperfunctions;
import com.squareup.picasso.Picasso;

/**
 * Created by ravis on 6/13/2017.
 */

public class Goal_AllgoalsAdapter extends RecyclerView.Adapter<Goal_AllgoalsAdapter.ViewHolder> {
    static Context context;
    static NewGoalPojo pojo;
    private static final String goalId = "goalid";


    public Goal_AllgoalsAdapter(Context context, NewGoalPojo pojo) {
        this.context = context;
        this.pojo = pojo;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardViewImage;
        public TextView cardheading;
        public TextView card_details;
        public CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            cardViewImage = (ImageView) itemView.findViewById(R.id.firstpagecardviewimage);
            cardheading = (TextView) itemView.findViewById(R.id.firstpagecardheading);
            card_details = (TextView) itemView.findViewById(R.id.firstpagecardtext);
            cardView = (CardView) itemView.findViewById(R.id.firstpageCardVIew);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(context, Goal_details.class);
                    helperfunctions.helperGoalPosition=pojo.arr.get(getAdapterPosition()).getLocal_goals_id();
                    in.putExtra(goalId, pojo.arr.get(getAdapterPosition()).getLocal_goals_id());
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(in);
                }
            });
        }
    }

    @Override
    public Goal_AllgoalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext()).inflate(R.layout.goal_a_allgoalscard, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.cardheading.setText(pojo.arr.get(position).getLocal_goal_name());
        holder.card_details.setText(pojo.arr.get(position).getLocal_current_amount() + "");

        if (Character.isDigit(pojo.arr.get(position).getLocal_imagelink().charAt(0))) {

            Picasso.with(context).load(Integer.valueOf(pojo.arr.get(position).getLocal_imagelink()))
                    .placeholder(R.drawable.ic_imageholder)
                    .error(R.drawable.ic_errorimage)
                    .into(holder.cardViewImage);
        }else{

            Picasso.with(context).load(pojo.arr.get(position).getLocal_imagelink())
                    .placeholder(R.drawable.ic_imageholder)
                    .error(R.drawable.ic_errorimage)
                    .into(holder.cardViewImage);
        }


    }


    @Override
    public int getItemCount() {
        return pojo.arr.size();
    }
}
