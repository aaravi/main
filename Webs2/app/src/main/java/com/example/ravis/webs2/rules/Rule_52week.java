package com.example.ravis.webs2.rules;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.goals.Goal_details;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import org.w3c.dom.Text;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_MERCHANT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_ORDER;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;

public class Rule_52week extends AppCompatActivity {

    CardView fundedbank, starton, orderthestart, goalselection;

    LayoutInflater layoutInflater;

    Button create52weekrulebutton;

    TextView linkk, goalcount;

    ImageView onback, ondelete;

    AlertDialogs alert;

    goalSelection objectForTowards;

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    public static String specialChars2;

    private Boolean create = false;

    private int field_triggering_bank, ruleid = 2;

    TextView weekvalue, ordervalue,f2CardDescription,f2Title,startOnText;

    Context context;

    private int check = 0, count = 0;

    final String[] valuefor_OrderOf_Deduction = new String[2];

    final String[] valuefor_WeekAmount_Deduction = new String[52];

    private int multiplier = 10;

    SqlTransactions transaction;

    boolean isclicked=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_b_52week);
        context = this;
        alert = new AlertDialogs(context);

        onback = (ImageView) findViewById(R.id.f2back);
        ondelete = (ImageView) findViewById(R.id.f2delete);

        orderthestart = (CardView) findViewById(R.id.f2order);
        starton = (CardView) findViewById(R.id.f2starton);
        fundedbank = (CardView) findViewById(R.id.f2fundedbank);
        goalselection = (CardView) findViewById(R.id.f2towardsgoal);
        objectForTowards = new goalSelection(context);
        goalcount = (TextView) findViewById(R.id.f2towardsgoaltext);
        ordervalue = (TextView) findViewById(R.id.f2ordertext);
        weekvalue = (TextView) findViewById(R.id.f2startontext);
        linkk = (TextView) findViewById(R.id.f2fundedbanktext);
f2CardDescription=(TextView)findViewById(R.id.f2CardDescription);
        f2Title=(TextView)findViewById(R.id.f2title);
        create52weekrulebutton = (Button) findViewById(R.id.f2button);

startOnText=(TextView)findViewById(R.id.f2startontext);

        create=this.getIntent().getBooleanExtra("type",true);
        ruleid=this.getIntent().getIntExtra("ruleid",1);
        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        bankval = new SqlTransactions(Rule_52week.this).fundingBankAccount();
        goalsdata = new SqlTransactions(Rule_52week.this).getallgoalsforrule(ruleid);
        create52weekrulebutton.setOnClickListener(create52weekruleListener);
        layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        fundedbank.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();
                        linkk.setText(x);
                        field_triggering_bank = bankval.get(which).getYodlee_id();
                    }
                });
            }
        });
//startOnText.addTextChangedListener(new helperfunctions().PageHeadingChanger(startOnText,ordervalue,null,f2CardDescription,""));

        //changes in deduction value per week and order of deduction edit text
        // ============================================================================
        // listener for deduction value per week according to order of deduction decided
        starton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                StringBuffer extractedvalue = new StringBuffer();
                char[] week = weekvalue.getText().toString().toCharArray();
                for (int i = 0; i < week.length; i++) {
                    while (Character.isDigit(week[i])) {
                        extractedvalue.append(week[i]);
                        i++;
                    }
                    if (week[i] == '-') break;
                }

                valuefor_OrderOf_Deduction[0] = "regular (Start with ₹" +
                                                (Integer.valueOf(String.valueOf(extractedvalue)) *
                                                 multiplier) + ")";
                valuefor_OrderOf_Deduction[1] = "reversed (start with ₹" + ((((52 * multiplier) -
                                                                              (Integer.valueOf(String.valueOf(extractedvalue)) *
                                                                               multiplier) +
                                                                              multiplier))) + ")";

                String ordertext = ordervalue.getText().toString();
                if (ordertext.toLowerCase().contains("regular"))
                    for (int i = 0; i < 52; i++) {
                        valuefor_WeekAmount_Deduction[i] =
                                "week " + (i + 1) + "-₹" + ((i + 1) * multiplier);
                    }
                else
                    for (int i = 0; i < 52; i++) {
                        valuefor_WeekAmount_Deduction[i] =
                                "week " + (i + 1) + "-₹" + ((52 * multiplier) - ((i) * multiplier));
                    }


                final classforsingleandmultiselect d = new classforsingleandmultiselect(Rule_52week.this, weekvalue, valuefor_WeekAmount_Deduction, "Select order", ordervalue, valuefor_OrderOf_Deduction);
                d.singlechoiceselection_forweek();

            }

        });

        //textewatcher class called to keep track of the change text so that
        //valuefor_WeekAmount_Deduction in arrays be updated accordingly==============================================
        final helperClass_for_WeekOfCreate52Week_rule object = new helperClass_for_WeekOfCreate52Week_rule();
        ordervalue.addTextChangedListener(object.set_value_in_array(weekvalue));

        //listener for decideing the order of the deduction and updating the valuefor_WeekAmount_Deduction in arrays for
        //week value and order value accordingly=====================
        orderthestart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (object.aftervalue_reversed == "" || object.aftervalue_regular == "") {
                     int extractedvalue=getWeekValue(weekvalue.getText().toString());
                    if (ordervalue.getText().toString().toLowerCase().contains("regular")) {
                        valuefor_OrderOf_Deduction[0] = "regular (Start with ₹" +
                                                        (extractedvalue *
                                                         multiplier) + ")";
                        valuefor_OrderOf_Deduction[1] =
                                "reversed (start with ₹" + ((((52 * multiplier) -
                                                              (extractedvalue *
                                                               multiplier) + multiplier))) +
                                ")";
                    } else {
                        valuefor_OrderOf_Deduction[0] =
                                "reversed (start with ₹" + ((((52 * multiplier) -
                                                              (extractedvalue *
                                                               multiplier) + multiplier))) +
                                ")";
                        valuefor_OrderOf_Deduction[1] = "regular (Start with ₹" +
                                                        (extractedvalue *
                                                         multiplier) + ")";
                    }
                } else {
                    valuefor_OrderOf_Deduction[0] = object.aftervalue_regular;
                    valuefor_OrderOf_Deduction[1] = object.aftervalue_reversed;
                }

                classforsingleandmultiselect d = new classforsingleandmultiselect(Rule_52week.this, ordervalue, valuefor_OrderOf_Deduction, "Select order", weekvalue);
                d.singlechoiceselection_fororder();
            }

        });

        if (create) {
            f2Title.setText("Create 52 Week Rule");
            create52weekrulebutton.setText("Create Rule");
            goalcount.setText(goalsdata==null?"Select Goals":(goalsdata.get(0).getGoal_name()));
            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);
                }
            });
        } else {
            ondelete.setVisibility(View.VISIBLE);
            ondelete.setOnClickListener(deleteRule);
            create52weekrulebutton.setOnClickListener(updateRule);
            create52weekrulebutton.setText("Save");
            f2Title.setText("Edit Rule");
            goalsdata = new SqlTransactions(Rule_52week.this).getallgoalsforrule(ruleid);

            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);

            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);



                }
            });
            loadAllValues();
        }
    }
    //function to get Week value
    //technically it gives "first digits" in String
    private int getWeekValue(String text) {

        StringBuffer extractedvalue = new StringBuffer();
        char[] week = text.toCharArray();
        for (int i = 0; i < week.length; i++) {
            while (Character.isDigit(week[i])) {
                extractedvalue.append(week[i]);
                i++;
            }
            if (week[i] == '-') break;
        }
        return Integer.parseInt(extractedvalue + "");
    }

    private ContentValues getValuesFromFields() {

        ContentValues dbvalues = new ContentValues();

        dbvalues.put(RULE_NAME, "52 Week Rule");

        dbvalues.put(RULE_IMAGE, R.drawable.img2);

        dbvalues.put(RULE_TRIGERRING_BANK, field_triggering_bank);

        dbvalues.put(RULE_SAVE_AMOUNT, getWeekValue(weekvalue.getText().toString()));

        dbvalues.put(RULE_MERCHANT, ordervalue.getText().toString());

        return dbvalues;
    }


    //works when Activity opens to create new Rule-----------------------------------------------------------------------------------------
    private Button.OnClickListener create52weekruleListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            objectForTowards.getGoalid(isclicked,goalsdata);

            Intent in = new Intent(context, helperfunctions.AfterNewRuleReurnToClass);

            transaction = new SqlTransactions(context);

            transaction.insertRule(getValuesFromFields());

            transaction.insertmappingcreateRule(objectForTowards.goalid);

            finish();

            startActivity(in);
        }
    };


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }


    //When Activity opens in Edit mode it loads previous values for that rule---------------------------------------------------------------
    private void loadAllValues() {
        // createrounduprulebutton.setText("Save");
        //  alert.finalselected_goals=new SqlTransactions(context).getallgoalsforrule(ruleid);

        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();//to move to fields

        int amount_in_db = Integer.parseInt(
                c.getString(2) == null ? "0" : c.getString(2));          //save amount


       String orderOfAmount_in_db= c.getString(5) == null ? "0" : c.getString(5);
        ordervalue.setText(orderOfAmount_in_db);
        if(orderOfAmount_in_db.toLowerCase().contains("regular"))
            weekvalue.setText("week"+amount_in_db+"- ₹"+(amount_in_db * multiplier));
        else
            weekvalue.setText("week"+amount_in_db+"- ₹"+(((52-amount_in_db)+1 )*multiplier));
        field_triggering_bank =
                c.getString(14) == null ? 0 : Integer.parseInt(c.getString(14));//id for funded bank

        //   triggeringbank.setText(c.getString(13) == null ? "Please Link Your Bank" : c.getString(13));//triggering bank

        linkk.setText(
                c.getString(15) == null ? "Please Link Your Bank" : c.getString(15));//funded bank


        // ofmorethan.setText(c.getString(8).equals("") ? "0" : "$" + c.getString(2) + ".00");//save amount

    }

    View.OnClickListener updateRule=new View.OnClickListener() {
        ArrayList<Integer> finalSelected_goals = new ArrayList<>();

        @Override
        public void onClick(View v) {

            if (objectForTowards.alert.finalselected_goals == null) {

                for (int i = 0; i < goalsdata.size(); i++) {

                    if (goalsdata.get(i).getRules_id() == ruleid)

                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }

            } else {

                finalSelected_goals = objectForTowards.alert.finalselected_goals;

            }

            new SqlTransactions(context).updateRuleWithMappings(getValuesFromFields(), ruleid, finalSelected_goals);

           finish();

        }
    };



    View.OnClickListener deleteRule = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

            dialog.setTitle("Confirm Delete");

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    long result = new SqlTransactions(context).deleteRule(ruleid);

                    Toast.makeText(context, result != -1 ? "Rule Deleted"
                                                         : "Error deleting rule", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).show();


        }
    };

}