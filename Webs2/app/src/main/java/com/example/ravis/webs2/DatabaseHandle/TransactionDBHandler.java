package com.example.ravis.webs2.DatabaseHandle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Vishal on 4/1/2017.
 */

public class TransactionDBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "transaction.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_TRANSACTION = "transactions";
    public static final String TABLE_HISTORY_BALANCE = "history_balance";
    public static final String TABLE_CATEGORY = "categories";
    public static final String TABLE_ACCOUNTS = "accounts";

    public static final String COLUMN_HISTORY_id = "id";
    public static final String COLUMN_HISTORY_yodlee_bank_accountId = "yodlee_bank_accountId";
    public static final String COLUMN_HISTORY_isAsset = "isAsset";
    public static final String COLUMN_HISTORY_asOfDate = "asOfDate";
    public static final String COLUMN_HISTORY_date = "date";
    public static final String COLUMN_HISTORY_balance_amount = "balance_amount";
    public static final String COLUMN_HISTORY_balance_currency = "balance_currency";
    public static final String COLUMN_HISTORY_dataSourceType = "dataSourceType";
    public static final String COLUMN_HISTORY_account_id = "account_id";
    public static final String COLUMN_HISTORY_created_at = "created_at";
    public static final String COLUMN_HISTORY_intervalz = "intervalz";

    private static final String TABLE_CREATE_DASH = "create table "
            + TABLE_HISTORY_BALANCE + "(" + COLUMN_HISTORY_id
            + " INTEGER PRIMARY KEY, "
            + COLUMN_HISTORY_yodlee_bank_accountId + " TEXT, "
            + COLUMN_HISTORY_isAsset + " INTEGER, "
            + COLUMN_HISTORY_asOfDate + " TEXT , "
            + COLUMN_HISTORY_date + " NUMERIC NOT NULL, "
            + COLUMN_HISTORY_balance_amount + " REAL, "
            + COLUMN_HISTORY_balance_currency + " TEXT, "
            + COLUMN_HISTORY_dataSourceType + " TEXT , "
            + COLUMN_HISTORY_account_id + " INTEGER , "
            + COLUMN_HISTORY_created_at + " NUMERIC NOT NULL, "
            + COLUMN_HISTORY_intervalz + " TEXT)";

    // Individual transactions table ----------------------------------------------------------
    public static final String COLUMN_TRANSACTION_id = "id";
    public static final String COLUMN_TRANSACTION_api_id = "api_id";
    public static final String COLUMN_TRANSACTION_amount = "amount";
    public static final String COLUMN_TRANSACTION_amount_currency = "amount_currency";
    public static final String COLUMN_TRANSACTION_basetype = "basetype";
    public static final String COLUMN_TRANSACTION_description_original = "description_original";
    public static final String COLUMN_TRANSACTION_description_simple = "description_simple";
    public static final String COLUMN_TRANSACTION_type = "type";
    public static final String COLUMN_TRANSACTION_subType = "subType";
    public static final String COLUMN_TRANSACTION_isManual = "isManual";
    public static final String COLUMN_TRANSACTION_transactionDate = "transactionDate";
    public static final String COLUMN_TRANSACTION_accountId = "accountId";
    public static final String COLUMN_TRANSACTION_runningBalance = "runningBalance";
    public static final String COLUMN_TRANSACTION_runningBalance_currency = "runningBalance_currency";
    public static final String COLUMN_TRANSACTION_merchant_id = "merchant_id";
    public static final String COLUMN_TRANSACTION_CREATED_created_at = "created_at";
    public static final String COLUMN_TRANSACTION_categoryId = "categoryId";
    public static final String COLUMN_TRANSACTION_category = "category";
    public static final String COLUMN_TRANSACTION_highLevelCategoryId = "highLevelCategoryId";
    public static final String COLUMN_TRANSACTION_highLevelCategoryName = "highLevelCategoryName";
    public static final String COLUMN_TRANSACTION_icon_url = "icon_url";

    private static final String TABLE_CREATE = "create table "
            + TABLE_TRANSACTION + "(" + COLUMN_TRANSACTION_id
            + " INTEGER PRIMARY KEY , "
            + COLUMN_TRANSACTION_api_id + " TEXT, "
            + COLUMN_TRANSACTION_amount + " REAL, "
            + COLUMN_TRANSACTION_amount_currency + " TEXT, "
            + COLUMN_TRANSACTION_basetype + " TEXT, "
            + COLUMN_TRANSACTION_description_original + " TEXT, "
            + COLUMN_TRANSACTION_description_simple + " TEXT, "
            + COLUMN_TRANSACTION_type + " TEXT, "
            + COLUMN_TRANSACTION_subType + " TEXT, "
            + COLUMN_TRANSACTION_isManual + " TEXT , "
            + COLUMN_TRANSACTION_transactionDate + " NUMERIC, "
            + COLUMN_TRANSACTION_accountId + " TEXT, "
            + COLUMN_TRANSACTION_runningBalance + " REAL, "
            + COLUMN_TRANSACTION_runningBalance_currency + " TEXT, "
            + COLUMN_TRANSACTION_merchant_id + " TEXT, "
            + COLUMN_TRANSACTION_CREATED_created_at + " NUMERIC, "
            + COLUMN_TRANSACTION_categoryId + " TEXT, "
            + COLUMN_TRANSACTION_category + " TEXT, "
            + COLUMN_TRANSACTION_highLevelCategoryId + " TEXT, "
            + COLUMN_TRANSACTION_highLevelCategoryName + " TEXT, "
            + COLUMN_TRANSACTION_icon_url + " TEXT)";

    //category types with icons table -----------------------------------------------
    public static final String COLUMN_CATEGORY_id = "id";
    public static final String COLUMN_CATEGORY_yodlee_id = "yodlee_id";
    public static final String COLUMN_CATEGORY_source = "source";
    public static final String COLUMN_CATEGORY_category = "category";
    public static final String COLUMN_CATEGORY_type = "type";
    public static final String COLUMN_CATEGORY_highLevelCategoryId = "highLevelCategoryId";
    public static final String COLUMN_CATEGORY_highLevelCategoryName = "highLevelCategoryName";
    public static final String COLUMN_CATEGORY_created_at = "created_at";
    public static final String COLUMN_CATEGORY_created_by = "created_by";
    public static final String COLUMN_CATEGORY_rule_id = "rule_id";
    public static final String COLUMN_CATEGORY_icon_url = "icon_url";
    public static final String COLUMN_CATEGORY_final_icon = "final_icon";

    private static final String TABLE_CREATE_CAT = "create table "
            + TABLE_CATEGORY + "(" + COLUMN_CATEGORY_id
            + " INTEGER PRIMARY KEY , "
            + COLUMN_CATEGORY_yodlee_id + " INTEGER, "
            + COLUMN_CATEGORY_source + " TEXT, "
            + COLUMN_CATEGORY_category + " TEXT, "
            + COLUMN_CATEGORY_type + " TEXT, "
            + COLUMN_CATEGORY_highLevelCategoryId + " TEXT, "
            + COLUMN_CATEGORY_highLevelCategoryName + " TEXT, "
            + COLUMN_CATEGORY_created_at + " TEXT, "
            + COLUMN_CATEGORY_created_by + " TEXT, "
            + COLUMN_CATEGORY_rule_id + " TEXT, "
            + COLUMN_CATEGORY_icon_url + " TEXT, "
            + COLUMN_CATEGORY_final_icon + " TEXT)";

    //b Accounts data -------------------------------------------------------
    public static final String COLUMN_ACCOUNTS_id = "id";
    public static final String COLUMN_ACCOUNTS_CONTAINER = "CONTAINER";
    public static final String COLUMN_ACCOUNTS_accountName = "accountName";
    public static final String COLUMN_ACCOUNTS_accountStatus = "accountStatus";
    public static final String COLUMN_ACCOUNTS_accountNumber = "accountNumber";
    public static final String COLUMN_ACCOUNTS_isAsset = "isAsset";
    public static final String COLUMN_ACCOUNTS_balance_amount = "balance_amount";
    public static final String COLUMN_ACCOUNTS_balance_currency = "balance_currency";
    public static final String COLUMN_ACCOUNTS_yodlee_id = "yodlee_id";
    public static final String COLUMN_ACCOUNTS_lastUpdated = "lastUpdated";
    public static final String COLUMN_ACCOUNTS_includeInNetWorth = "includeInNetWorth";
    public static final String COLUMN_ACCOUNTS_providerId = "providerId";
    public static final String COLUMN_ACCOUNTS_providerName = "providerName";
    public static final String COLUMN_ACCOUNTS_currentBalance_amount = "currentBalance_amount";
    public static final String COLUMN_ACCOUNTS_currentBalance_currency = "currentBalance_currency";
    public static final String COLUMN_ACCOUNTS_accountType = "accountType";
    public static final String COLUMN_ACCOUNTS_refresh_statusMessage = "refresh_statusMessage";
    public static final String COLUMN_ACCOUNTS_status = "status";
    public static final String COLUMN_ACCOUNTS_icon_url = "icon_url";

    private static final String TABLE_CREATE_ACCOUNT = "create table "
            + TABLE_ACCOUNTS + "(" + COLUMN_ACCOUNTS_id
            + " INTEGER PRIMARY KEY , "
            + COLUMN_ACCOUNTS_CONTAINER + " TEXT, "
            + COLUMN_ACCOUNTS_accountName + " TEXT, "
            + COLUMN_ACCOUNTS_accountStatus + " TEXT, "
            + COLUMN_ACCOUNTS_accountNumber + " INTEGER, "
            + COLUMN_ACCOUNTS_isAsset + " INTEGER, "
            + COLUMN_ACCOUNTS_balance_amount + " REAL, "
            + COLUMN_ACCOUNTS_balance_currency + " TEXT, "
            + COLUMN_ACCOUNTS_yodlee_id + " INTEGER, "
            + COLUMN_ACCOUNTS_lastUpdated + " NUMERIC , "
            + COLUMN_ACCOUNTS_includeInNetWorth + " INTEGER, "
            + COLUMN_ACCOUNTS_providerId + " INTEGER, "
            + COLUMN_ACCOUNTS_providerName + " TEXT, "
            + COLUMN_ACCOUNTS_currentBalance_amount + " REAL, "
            + COLUMN_ACCOUNTS_currentBalance_currency + " TEXT, "
            + COLUMN_ACCOUNTS_accountType + " TEXT, "
            + COLUMN_ACCOUNTS_refresh_statusMessage + " TEXT, "
            + COLUMN_ACCOUNTS_status + " TEXT, "
            + COLUMN_ACCOUNTS_icon_url + " TEXT)";


    public TransactionDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
        db.execSQL(TABLE_CREATE_DASH);
        db.execSQL(TABLE_CREATE_CAT);
        db.execSQL(TABLE_CREATE_ACCOUNT);

        Log.d("TABLE_CREATE_CAT", TABLE_CREATE_CAT);
        Log.d("TABLE_CREATE_ACCOUNT", TABLE_CREATE_ACCOUNT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSACTION);
        db.execSQL(TABLE_CREATE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY_BALANCE);
        db.execSQL(TABLE_CREATE_DASH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL(TABLE_CREATE_CAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNTS);
        db.execSQL(TABLE_CREATE_ACCOUNT);
    }




}
