package com.example.ravis.webs2.goals;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.CustomCover;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Goal_changeCover extends AppCompatActivity {
    List<CustomCover> imageDetails;
    ImageView backButton,clearButton;
    ProgressBar load;
    LinearLayout error;
    RecyclerView views;
    EditText forsearch;
    Goal_changeCoverAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.goal_z_changecover);
        imageDetails=new ArrayList<>();
        load=(ProgressBar)findViewById(R.id.loadRing);
        error=(LinearLayout)findViewById(R.id.errorDisplay);
        clearButton=(ImageView)findViewById(R.id.clear);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forsearch.setText("");
            }
        });
        backButton = (ImageView) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        forsearch=(EditText)findViewById(R.id.searchin);
        forsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    imageDetails.clear();
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(forsearch.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    performSearch(forsearch.getText().toString());
                    load.setVisibility(View.VISIBLE);
                    error.setVisibility(View.GONE);
                    return true;
                }
                return false;
            }
        });

        views=(RecyclerView)findViewById(R.id.imageGrid);
        views.setLayoutManager(new GridLayoutManager(this,3));

    }

    void performSearch(final String searchkey){

        Goal_changeCoverService serve= Goal_changeCoverClient.getImageService();

        Call<List<CustomCover>> call=serve.getImageList(searchkey);


        call.enqueue(new Callback<List<CustomCover>>() {
            @Override
            public void onResponse(Call<List<CustomCover>> call, Response<List<CustomCover>> response) {
                load.setVisibility(View.GONE);
                if(response.isSuccessful()) {

                    imageDetails = response.body();
                    adapter=new Goal_changeCoverAdapter(Goal_changeCover.this,imageDetails,searchkey);
                    views.setAdapter(adapter);

                } else {
                    error.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<List<CustomCover>> call, Throwable t) {
                error.setVisibility(View.VISIBLE);
                load.setVisibility(View.GONE);
            }
        });


    }



}
