package com.example.ravis.webs2.goals;

import com.example.ravis.webs2.goals.Goal_changeCoverService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Kshitij Mittal on 03-07-2017.
 */

public class Goal_changeCoverClient {

    private static final String ROOT_URL = "http://app.capitally.in/webservices/image_search/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static Goal_changeCoverService getImageService() {
        return getRetrofitInstance().create(Goal_changeCoverService.class);
    }



}
