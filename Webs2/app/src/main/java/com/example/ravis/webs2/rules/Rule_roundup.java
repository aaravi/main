package com.example.ravis.webs2.rules;


import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goals.Goal_details;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.pojo.RuleFileds;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;


public class Rule_roundup extends AppCompatActivity {

    CardView using_linkedbanks, roundupamount, goalselection, yourLinkedBanks;

    LayoutInflater layoutInflater;

    Context context;

    Button createrounduprulebutton;

    ImageView ondelete,onback;

    TextView triggeringbank, yourfunded, roundupvalue, goalcount, heading, roundupRuleHeading;

    AlertDialogs alert;

    goalSelection objectForTowards;

    classforsingleandmultiselect amountvalue = new classforsingleandmultiselect(this);

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    RuleFileds rule;

    int int_usingBank_Slected = 0;

    float selected_roundupto;

    classforsingleandmultiselect d;

    int int_towardsBank_Selected = 0, ruleid = 2;

    SqlTransactions transaction;

    BroadcastReceiver mReceiver;

    ArrayList<Integer> goalid = new ArrayList<Integer>();

    Boolean create = true;

    int ruleid_coming = 20;

    private int check = 0, count = 0;

    boolean isEdited = false,isclicked=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.rule_b_roundup);

        context = Rule_roundup.this;

        alert = new AlertDialogs(context);

        objectForTowards = new goalSelection(context);

        //all findViewById
        onback = (ImageView) findViewById(R.id.ruback);
        ondelete = (ImageView) findViewById(R.id.rudelete);

        goalselection = (CardView) findViewById(R.id.rutowardsgoal);
        yourLinkedBanks = (CardView) findViewById(R.id.rufundedbank);
        using_linkedbanks = (CardView) findViewById(R.id.ruusingbank);
        roundupamount = (CardView) findViewById(R.id.ruupto);

        yourfunded = (TextView) findViewById(R.id.rufundedbanktext);
        roundupRuleHeading = (TextView) findViewById(R.id.rutitle);
        roundupvalue = (TextView) findViewById(R.id.ruuptotext);
        triggeringbank = (TextView) findViewById(R.id.ruusingbanktext);
        goalcount = (TextView) findViewById(R.id.rutowardsgoaltext);
        heading = (TextView) findViewById(R.id.ruCardDescription);

        createrounduprulebutton = (Button) findViewById(R.id.rubutton);

        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        bankval = new ArrayList<>();
        goalsdata = new ArrayList<>();
        bankval = new SqlTransactions(Rule_roundup.this).fundingBankAccount();
        goalsdata = new SqlTransactions(Rule_roundup.this).getallgoalsforrule(ruleid);
        create = this.getIntent().getBooleanExtra("type", false);
        ruleid = this.getIntent().getIntExtra("ruleid", 1);

        roundupvalue.addTextChangedListener(new helperfunctions().PageHeadingChanger(roundupvalue, null, null, heading, "roundup"));

        using_linkedbanks.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;

                alert.SingleSelectWithImage("Select Triggering Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();

                        int_usingBank_Slected = bankval.get(which).getYodlee_id();

                        triggeringbank.setText(x);
                    }
                });
            }
        });
        yourLinkedBanks.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;

                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();

                        int_towardsBank_Selected = bankval.get(which).getYodlee_id();

                        yourfunded.setText(x);
                    }
                });
            }
        });
        final String[] values = {"₹1.00", "₹2.00", "₹3.00", "₹4.00", "₹5.00"};

        roundupamount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;

                classforsingleandmultiselect d = new classforsingleandmultiselect(Rule_roundup.this, roundupvalue, values, "Round up to");

                d.singlechoiceselection();

            }

        });

        //activity working to create new rule--------------------------------------------------------
        if (create == true) {
            ondelete.setVisibility(View.INVISIBLE);

            createrounduprulebutton.setOnClickListener(createNewuleListner);

            createrounduprulebutton.setText("Create Rule");

            roundupRuleHeading.setText("Create Round Up Rule");

            goalcount.setText(goalsdata == null ? "Select Goals" : (goalsdata.get(0).getGoal_name()));

            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isEdited = true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });

            //activity to edit existing rule--------------------------------------------------------
        } else {

            goalsdata = new SqlTransactions(Rule_roundup.this).getallgoalsforrule(ruleid);

            roundupRuleHeading.setText("Edit Rule");

            loadValuestoTheirFields();

            createrounduprulebutton.setOnClickListener(UpdateRule);

            ondelete.setVisibility(View.VISIBLE);

            ondelete.setOnClickListener(deleteRule);

            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);

            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isEdited = true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);
                }
            });
        }

    }
    @Override
    public void onBackPressed() {


        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

        dialog.setTitle("Discard Changes?");

        if (isEdited)

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Rule_roundup.this.onBackPressed();//long result=new SqlTransactions(context).deleteRule(ruleid)

                    Rule_roundup.this.finish();

                }
            }).show();
        else

            finish();

    }

    private void editRoundup() {

    }

    //get all selected Fields from this rule and add them to ContentValue Object -----------------------------------
    private ContentValues getAllValuesFromFields() {

        ContentValues dbvalues = new ContentValues();

        dbvalues.put(RULE_NAME, "Round Up Rule");

        dbvalues.put(RULE_IMAGE, R.drawable.img2);

        dbvalues.put(RULE_TRIGERRING_BANK, int_usingBank_Slected);

        dbvalues.put(RULE_FUNDED_BANK, int_towardsBank_Selected);
        //dbvalues.put(GOAL_ID, String.valueOf(goalid));

        String stringValfromRounpUpValue = roundupvalue.getText().toString().replaceAll("[A-Z?i]|[₹]|[\\s]|", "").replace("\"", "");

        selected_roundupto = Float.valueOf(stringValfromRounpUpValue.replace("\"", ""));

        dbvalues.put(RULE_SAVE_AMOUNT, selected_roundupto);

        return dbvalues;
    }

    //when loading the page get Value from Database and set them to their respective fields---------------------------------
    private void loadValuestoTheirFields() {

        createrounduprulebutton.setText("Save");
        //  alert.finalselected_goals=new SqlTransactions(context).getallgoalsforrule(ruleid);


        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();//to move to fields

        roundupvalue.setText(
                c.getString(2).equals("") ? "0" : "₹" + c.getString(2) + ".00");//save amount

        int_usingBank_Slected =
                c.getString(12) == null ? 0 : Integer.parseInt(c.getString(12));//id for triggering bank


        triggeringbank.setText(c.getString(13) == null ? "Link Bank"
                : c.getString(13));//triggering bank name

        yourfunded.setText(

                c.getString(14) == null ? "Link Bank" : c.getString(14));//funded bank name

        int_towardsBank_Selected = c.getString(15) == null ? 0 : Integer.parseInt(c.getString(15)); //funded bank id

    }

    //listner for update Rule------------------------------------------------------------------------------------------
    private Button.OnClickListener UpdateRule = new View.OnClickListener() {

        ArrayList<Integer> finalSelected_goals = new ArrayList<>();

        @Override
        public void onClick(View v) {

            if (objectForTowards.alert.finalselected_goals== null) {

                for (int i = 0; i < goalsdata.size(); i++) {

                    if (goalsdata.get(i).getRules_id() == ruleid)

                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }

            } else {

                finalSelected_goals =objectForTowards.alert.finalselected_goals;

            }

            new SqlTransactions(context).updateRuleWithMappings(getAllValuesFromFields(), ruleid, finalSelected_goals);

            Rule_roundup.this.finish();

        }
    };

    View.OnClickListener deleteRule = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

            dialog.setTitle("Confirm Delete");

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    long result=new SqlTransactions(context).deleteRule(ruleid);

                    Toast.makeText(context, 1 != -1 ? "Rule Deleted"
                            : "Error deleting rule", Toast.LENGTH_SHORT).show();
                    Intent in=new Intent(context,Goal_details.class);
                    Rule_roundup.this.finish();
                    startActivity(in);
                }
            }).show();

        }
    };

    Button.OnClickListener createNewuleListner = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            objectForTowards.getGoalid(isclicked,goalsdata);

            Intent in = new Intent(Rule_roundup.this, helperfunctions.AfterNewRuleReurnToClass);

            transaction = new SqlTransactions(Rule_roundup.this);

            transaction.insertRule(getAllValuesFromFields());

            transaction.insertmappingcreateRule(objectForTowards.goalid);

            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            in.putExtra("position", helperfunctions.helperGoalPosition);

            finish();

            startActivity(in);
        }
    };

}