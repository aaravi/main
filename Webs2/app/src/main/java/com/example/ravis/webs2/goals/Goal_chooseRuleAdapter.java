package com.example.ravis.webs2.goals;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.rules_and_accounts;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by aasthu on 01-Jul-17.
 */

public class Goal_chooseRuleAdapter extends RecyclerView.Adapter<Goal_chooseRuleAdapter.viewHolder> {

    private ArrayList<rules_and_accounts> rulesdata;
    private Context context;
    private ArrayList<Integer> preSelected = new ArrayList<>();
    private boolean[] getting_selected_rules;

    Goal_chooseRuleAdapter(ArrayList<rules_and_accounts> rulesdata, Context context,ArrayList<Integer> preSelected) {

        this.rulesdata = rulesdata;
        this.context = context;
        getting_selected_rules = new boolean[rulesdata.size()];
        int x=0;
        for (rules_and_accounts r: rulesdata)
        {  if(preSelected!=null)
            for (int i=0;i<preSelected.size();i++){
                if(r.getId()==preSelected.get(i)){
                    getting_selected_rules[x]=true;
                }
            }

            x++;}
    }

    @Override
    public Goal_chooseRuleAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.goal_z_selectrulescard, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(final viewHolder holder,int position) {

        Picasso.with(context)
                .load(rulesdata.get(position).getRuleimage())
                .placeholder(R.drawable.ic_imageholder)
                .error(R.drawable.ic_errorimage)
                .into(holder.ruleimage);
        Picasso.with(context)
                .load(rulesdata.get(position).getBankIcon())
                .placeholder(R.drawable.ic_imageholder)
                .error(R.drawable.ic_errorimage)
                .into(holder.bankimage);


        holder.rulename.setText(rulesdata.get(position).getRulename());
        holder.amount.setText("Round up to " + rulesdata.get(position).getAmount());

        holder.valuececk.setChecked(getting_selected_rules[holder.getAdapterPosition()]);

        holder.valuececk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    getting_selected_rules[holder.getAdapterPosition()] = isChecked;


            }
        });
    }


    @Override
    public int getItemCount() {
        return rulesdata.size();
    }

    public ArrayList<Integer> getselected_ruleid() {

        for (int i = 0; i < rulesdata.size(); i++) {
            if (getting_selected_rules[i])
                preSelected.add(rulesdata.get(i).getId());
        }
        return preSelected;
    }


    public class viewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout cardView;
        ImageView ruleimage, bankimage;
        TextView rulename, amount;
        CheckBox valuececk;

        public viewHolder(View itemView) {

            super(itemView);
            cardView = (ConstraintLayout) itemView.findViewById(R.id.mainCard);
            ruleimage = (ImageView) itemView.findViewById(R.id.ruleimage);
            bankimage = (ImageView) itemView.findViewById(R.id.bankimage);
            rulename = (TextView) itemView.findViewById(R.id.rulename);
            amount = (TextView) itemView.findViewById(R.id.datafromrule);
            valuececk = (CheckBox) itemView.findViewById(R.id.valuecheck);

            cardView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                        valuececk.toggle();
                        getting_selected_rules[getAdapterPosition()] =valuececk.isChecked();

                }
            });

        }
    }

}
