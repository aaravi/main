package com.example.ravis.webs2.rules;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravis.webs2.R;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MultiSelectAdapter extends ArrayAdapter
{

    private int[] imageresource;

    ArrayList<TowardsBankAccount> item;

    static ArrayList<goals_with_goalsof_particular_rule> item1;

    private static boolean[] isselected = {false, false, false, false, false, false, false, false};

    private boolean ischeckboxneeded, count;

    Context context;

    int selectedbank = 0, a = 0;

    ArrayList<Boolean> aa;

    SparseBooleanArray mCheckStates;

    CheckBox select;


    public MultiSelectAdapter(Context context)
    {

        super(context, 0);
    }


    public MultiSelectAdapter(Context context, ArrayList<TowardsBankAccount> item)
    {

        super(context, 0, item);
        this.context=context;
        this.item = item;
        ischeckboxneeded = false;
    }

    public MultiSelectAdapter(Context context, ArrayList<goals_with_goalsof_particular_rule> item, int a)
    {

        super(context, 0, item);

        this.item1 = item;

        this.context = context;

        ischeckboxneeded = true;

        this.a = a;


    }


    public static void setIsselected()
    {

        for (int i = 0; i < item1.size(); i++)
        {
            if (item1.get(i).getRules_id() != 0)

                isselected[i] = true;

            else

                isselected[i] = false;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {


        if (convertView == null)
        {

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_multiselectview, parent, false);

        }

        TextView name = (TextView) convertView.findViewById(R.id.textaccordingtorule);

        ImageView icon = (ImageView) convertView.findViewById(R.id.setmultiimage);

        select = (CheckBox) convertView.findViewById(R.id.checkBoxtobe);


        if (ischeckboxneeded)

        {
            select.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {

                @Override
                synchronized public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {

                    isselected[position] = isChecked;
                }
            });

            name.setText((item1.get(position).getGoal_name()));//getItem(position)

            Picasso.with(context)
                    .load(item1.get(position).getImagelink())
                    .placeholder(R.drawable.img1)
                    .error(android.R.drawable.stat_notify_error)
                    .into(icon);


            if (position == 0)

                select.setChecked(false);

            setIsselected();

            if (isselected[position] == true)

                select.setChecked(true);

        } else
        {
            name.setText((item.get(position).getBankName()));//getItem(position)

            selectedbank = item.get(position).getYodlee_id();

            Picasso.with(context)
                    .load(item.get(position).getBankIcon())
                    .placeholder(R.drawable.img1)
                    .error(android.R.drawable.stat_notify_error)
                    .into(icon);

            select.setVisibility(View.INVISIBLE);

        }

        return convertView;
    }


    public ArrayList<Boolean> getselectedList()
    {

        aa = new ArrayList<Boolean>();


        for (int i = 0; i < item1.size(); i++)

        {
            aa.add(isselected[i]);
        }
        //Toast.makeText(context, aa+"", Toast.LENGTH_SHORT).show();
        return aa;
    }
}