package com.example.ravis.webs2;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;
import com.example.ravis.webs2.rules.AlertDialogs;
import com.example.ravis.webs2.rules.Rule_roundup;

import java.util.ArrayList;

/**
 * Created by aasthu on 22-Jul-17.
 */

public class goalSelection {
    Context context;
    public ArrayList<Integer> goalid = new ArrayList<Integer>();
    public AlertDialogs alert;
    static int check = 0;
    int count = 0;

    public goalSelection(Context context) {
        this.context = context;
        alert = new AlertDialogs(context);


    }

    public ArrayList<Integer> towardgoalshelper(ArrayList<goals_with_goalsof_particular_rule> goalsdata, TextView goalcount, boolean create, int ruleid) {

        if (create) {

            if (goalsdata != null) {
                alert.MultiSelectDialogCheck("Select Goals", goalsdata, goalcount, create);
            } else {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

                dialog.setTitle("Select Goals");

                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setMessage("No goals to Select").show();

            }
        } else {
            if (check++ == 0) {

                goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);

                alert.MultiSelectDialogCheck("goal", goalsdata, goalcount, create);

            } else {

                for (int i = 0; i < goalsdata.size(); i++) {

                    goalsdata.get(i).setGoals_id(goalsdata.get(i).getGoals_id());

                    goalsdata.get(i).setRules_id(goalsdata.get(i).getRules_id());

                    goalsdata.get(i).setGoal_name(goalsdata.get(i).getGoal_name());

                    goalsdata.get(i).setImagelink(goalsdata.get(i).getImagelink());

                    goalsdata.get(i).setCurrent_amount(goalsdata.get(i).getCurrent_amount());

                }

                alert.MultiSelectDialogCheck("goal", goalsdata, goalcount, create);
            }
        }


        return alert.finalselected_goals;
    }


    public ArrayList<Integer> getGoalid(boolean isclicked, ArrayList<goals_with_goalsof_particular_rule> goalsdata) {
        if (isclicked == false) {
            if (goalsdata != null)
                goalid.add(goalsdata.get(0).getGoals_id());

        } else {
            goalid = alert.finalselected_goals;
        }
        return goalid;
    }

    public void setTextGoalsCount(ArrayList<goals_with_goalsof_particular_rule> goalsdata, TextView goalcount, int ruleid) {
        for (int i = 0; i < goalsdata.size(); i++) {

            if (goalsdata.get(i).getRules_id() == ruleid) {
                count++;

            }
        }
        if (count == 0)

            goalcount.setText("Select goals");

        else if (count == 1) {

            for (int i = 0; i < goalsdata.size(); i++) {

                if (goalsdata.get(i).getRules_id() == ruleid) {

                    goalcount.setText("" + goalsdata.get(i).getGoal_name());

                }

            }
        } else

            goalcount.setText(count + " goals");

    }


}
