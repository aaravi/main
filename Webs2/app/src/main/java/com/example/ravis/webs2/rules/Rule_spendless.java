package com.example.ravis.webs2.rules;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ravis.webs2.DatabaseHandle.SqlTransactions;
import com.example.ravis.webs2.R;
import com.example.ravis.webs2.goalSelection;
import com.example.ravis.webs2.pojo.TowardsBankAccount;
import com.example.ravis.webs2.pojo.goals_with_goalsof_particular_rule;

import java.util.ArrayList;

import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_DURATION;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_FUNDED_BANK;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_IMAGE;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_MERCHANT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_NAME;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_SAVE_AMOUNT;
import static com.example.ravis.webs2.DatabaseHandle.DatabaseCreator.RULE_TRIGERRING_BANK;

/**
 * Created by aasthu on 14-Jun-17.
 */

public class Rule_spendless extends AppCompatActivity {

    static Boolean firstclick_morethan = true, firstclick_save = true;

    CardView spendlesslayout, perweeklayout, pickyourplace, using, yourss, goalselection;

    TextView spendlessthantext, linkedbanks, fundedbank, merchantText, spendlessRuleHeading, spendLessPageHeading, goalcount;

    ImageView ondelete, onback;

    Context context;

    Dialog builder;

    TextView dialog_title, dialogmessage, goalscounter;

    TextView week;

    EditText dialogedittext;

    ListView lv;

    goalSelection objectForTowards;

    private AlertDialogs alert;

    private Boolean create = false, isEdited = false,isclicked=false;;

    private int int_triggering_bank;

    private int int_funded_bank;

    private int ruleid = 30;

    ArrayList<TowardsBankAccount> bankval;

    ArrayList<goals_with_goalsof_particular_rule> goalsdata;

    private Button okbutton, cancelbutton, spendlessrulebutton;

    private int check = 0, count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.rule_b_spendless);
context=this;
        alert=new AlertDialogs(context);
        onback = (ImageView) findViewById(R.id.slback);
        ondelete = (ImageView) findViewById(R.id.sldelete);
        objectForTowards = new goalSelection(context);
        spendlesslayout = (CardView) findViewById(R.id.sllessthan);
        pickyourplace = (CardView) findViewById(R.id.slmerchant);
        perweeklayout = (CardView) findViewById(R.id.slper);
        using = (CardView) findViewById(R.id.slusing);
        yourss = (CardView) findViewById(R.id.slfundedbank);
        //towardyourbamk = (CardView) findViewById(R.id.towardsyourbank);
        goalselection = (CardView) findViewById(R.id.sltowardsgoal);

        //goalscounter = (TextView) findViewById(R.id.selectedgoalscount);
        week = (TextView) findViewById(R.id.slpertext);
        spendlessRuleHeading = (TextView) findViewById(R.id.slCardDescription);
        spendlessthantext = (TextView) findViewById(R.id.sllessthantext);
        fundedbank = (TextView) findViewById(R.id.slfundedbanktext);
        spendLessPageHeading = (TextView) findViewById(R.id.sltitle);
        linkedbanks = (TextView) findViewById(R.id.slusingtext);
        goalcount = (TextView) findViewById(R.id.sltowardsgoaltext);
        merchantText = (TextView) findViewById(R.id.slmerchanttext);

        spendlessrulebutton = (Button) findViewById(R.id.slbutton);

        onback.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        bankval = new SqlTransactions(Rule_spendless.this).fundingBankAccount();

        goalsdata = new SqlTransactions(Rule_spendless.this).getallgoalsforrule(2);

        create = this.getIntent().getBooleanExtra("type", true);

        ruleid = this.getIntent().getIntExtra("ruleid", 60);

        week.addTextChangedListener(new helperfunctions().PageHeadingChanger(week, spendlessthantext, merchantText, spendlessRuleHeading, "spendless"));

        spendlessthantext.addTextChangedListener(new helperfunctions().PageHeadingChanger(week, spendlessthantext, merchantText, spendlessRuleHeading, "spendless"));

        merchantText.addTextChangedListener(new helperfunctions().PageHeadingChanger(week, spendlessthantext, merchantText, spendlessRuleHeading, "spendless"));

        spendlessthantext.setText("₹100");


        //Initializes Dialog box with Edit Text---------------------------------------------------------------

        createDialogBox();

//        spendlessthantext.addTextChangedListener(new helperfunctions().setHeadingAccordingToView(spendlessthantext, spendlessRuleHeading, "Spend less than", "at " + merchantText.getText().toString() + " during a " + week.getText().toString() + " and save the difference"));
//
//        week.addTextChangedListener(new helperfunctions().setHeadingAccordingToView(week, spendlessRuleHeading, "Spend less than " + spendlessthantext.getText().toString() + " during a", " and save the difference"));

        spendlesslayout.setOnClickListener(spendlessListnerLayout);

        spendlessrulebutton.setOnClickListener(createSpendless);


        final String[] val = {"week", "month"};

        perweeklayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;
                classforsingleandmultiselect d = new classforsingleandmultiselect(Rule_spendless.this, week, val, "Select Interval");

                d.singlechoiceselection();

            }

        });


        pickyourplace.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;
                Intent i = new Intent(Rule_spendless.this, Rule_SelectMerchant.class);
                startActivity(i);
            }
        });


        using.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;
                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();

                        int_triggering_bank = bankval.get(which).getYodlee_id();

                        linkedbanks.setText(x);
                    }
                });
            }
        });

        yourss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isEdited = true;
                alert.SingleSelectWithImage("Select Funded Bank", bankval, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String x = bankval.get(which).getBankName();

                        int_funded_bank = bankval.get(which).getYodlee_id();

                        fundedbank.setText(x);
                    }
                });
            }
        });
        if (create) {


            spendlessrulebutton.setText("Create Rule");

            spendLessPageHeading.setText("Create Spendless Rule");

            spendlessrulebutton.setOnClickListener(createSpendless);

            ondelete.setVisibility(View.INVISIBLE);

            goalcount.setText(goalsdata==null?"Select Goals":(goalsdata.get(0).getGoal_name()));


            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isEdited = true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });

        } else {
            goalsdata = new SqlTransactions(context).getallgoalsforrule(ruleid);
            for (int i = 0; i < goalsdata.size(); i++)
                Toast.makeText(context,
                        "" + goalsdata.get(i).getRules_id(), Toast.LENGTH_LONG).show();

            spendlessrulebutton.setText("Save");

            spendLessPageHeading.setText("Edit Rule");

            ondelete.setOnClickListener(deleteRule);

            loadAllValues();

            ondelete.setVisibility(View.VISIBLE);

            spendlessrulebutton.setOnClickListener(updateRule);


            objectForTowards.setTextGoalsCount(goalsdata,goalcount,ruleid);
            goalselection.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    isEdited = true;

                    isclicked = true;

                    objectForTowards.towardgoalshelper(goalsdata, goalcount, create, ruleid);

                }
            });
        }
//When a user clicks on spendless field new valuefor_WeekAmount_Deduction are set each time in dialog box------------------------------------------------------------


    }

    //loads All valuefor_WeekAmount_Deduction to their respective fields------------------------------------------------------------------------------------
    private void loadAllValues() {

        Cursor c = new SqlTransactions(context).getRuledataWithBankName(ruleid);

        c.moveToNext();

        spendlessthantext.setText(c.getString(2));
//        if (c.getString(8).equals("0"))
//            week.setText("Week");
//        else
//            week.setText("Month");
        //week.setText((c.getString(8).equals("0")?"Week".toString():"Month".toString()));

        spendlessthantext.setText("₹" +
                                  c.getString(2));                                                  //value for spendless than field

        week.setText((c.getString(5).equals("0") ? "week".toString()
                                                 : "month".toString()));        //value for "per" field

        merchantText.setText(c.getString(5));                                                       //merchant ("at" field)

        int_triggering_bank = Integer.parseInt(
                c.getString(12) == null ? "0" : c.getString(12));       //id for triggering bank

        linkedbanks.setText(c.getString(13));                                                       //triggering bank

        fundedbank.setText(c.getString(14));                                                            //funded bank

        int_funded_bank = Integer.parseInt(
                c.getString(15) == null ? "0" : c.getString(15));              //funded bank id

    }

    //creates new Dialog box------------------------------------------------------------------------------------------------------
    private void createDialogBox()

    {

        alert = new AlertDialogs(Rule_spendless.this);

        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.z_customdialog_for_checkbox, null);

        builder = new Dialog(context);

        builder.setContentView(view);

        okbutton = (Button) builder.findViewById(R.id.custom_ok);

        cancelbutton = (Button) builder.findViewById(R.id.custom_cancle);

        dialog_title = (TextView) builder.findViewById(R.id.custom_dialog_title);

        dialogmessage = (TextView) builder.findViewById(R.id.custom_dialog_message);

        lv = (ListView) builder.findViewById(R.id.custom_listView);

        dialogedittext = (EditText) builder.findViewById(R.id.dialog_edit_text);

        dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));

        dialogedittext.setTextColor(Color.parseColor("#000000"));

        okbutton.setTextColor(Color.parseColor("#b8d7e0"));

        dialogedittext.setHintTextColor(Color.parseColor("#808080"));
    }

    //functionality to be performed when user presses back button-------------------------------------------------------------------
    @Override
    public void onBackPressed() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);

        dialog.setTitle("Discard Changes?");

        if (isEdited)

            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // rounduprule.this.onBackPressed();//long result=new SqlTransactions(context).deleteRule(ruleid)

                    Rule_spendless.this.finish();

                }
            }).show();

        else

            finish();

    }

    //get selected valuefor_WeekAmount_Deduction from their respective fields and them to contentvalues object-------------------------------------------

    private ContentValues getValuesFromFields()

    {

        ContentValues dbvalues = new ContentValues();

        dbvalues.put(RULE_NAME, "Spend Less Rule");

        dbvalues.put(RULE_IMAGE, R.drawable.img2);

        dbvalues.put(RULE_TRIGERRING_BANK, int_triggering_bank);

        dbvalues.put(RULE_FUNDED_BANK, int_funded_bank);

        String stringValfromRounpUpValue = spendlessthantext.getText().toString().replaceAll("[₹]", "").replace("\"", "");
        int selected_amount = Integer.parseInt(stringValfromRounpUpValue.replace("\"", ""));
        dbvalues.put(RULE_SAVE_AMOUNT, selected_amount);

        dbvalues.put(RULE_DURATION, week.getText().toString().equalsIgnoreCase("week") ? 0 : 1);

        dbvalues.put(RULE_MERCHANT, merchantText.getText().toString());

        return dbvalues;
    }

    View.OnClickListener spendlessListnerLayout = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            isEdited = true;

            final helperfunctions helperobject = new helperfunctions();

            dialogedittext.setHint(spendlessthantext.getText());

            okbutton.setEnabled(false);

            dialogmessage.setTextColor(Color.parseColor("#1FA8CE"));

            dialogedittext.setTextColor(Color.parseColor("#000000"));

            okbutton.setTextColor(Color.parseColor("#b8d7e0"));

            dialogedittext.setKeyListener(DigitsKeyListener.getInstance(false, false));

            dialogedittext.setVisibility(View.VISIBLE);

            helperobject.inputlimiter(dialogedittext, 4);

            helperobject.limittovalue(5000, 3, dialogedittext, okbutton);

            dialog_title.setText("Spend less than");

            dialogmessage.setText("Amount(max ₹5,000)");

            builder.show();

            cancelbutton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    builder.cancel();

                    dialogedittext.setText("");
                }
            });
            okbutton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    spendlessthantext.setText("₹" + dialogedittext.getText().toString());

                    dialogedittext.setText("");

                    builder.cancel();
                }
            });
        }
    };
    //Listner when a new spendless rule is created-----------------------------------------------------------------------------------------

    View.OnClickListener createSpendless = new View.OnClickListener() {

        long result;

        @Override
        public void onClick(View v) {

            objectForTowards.getGoalid(isclicked,goalsdata);


            result = new SqlTransactions(context).insertRule(getValuesFromFields());

            Toast.makeText(context, result != -1 ? "New Rule created"
                                                 : "Error creating rule", Toast.LENGTH_SHORT).show();

            new SqlTransactions(context).insertmappingcreateRule(objectForTowards.goalid);

            Toast.makeText(context, result != -1 ? "Mappings Updated"
                                                 : "Error updataing mappings", Toast.LENGTH_SHORT).show();

            if (result != -1)

            {

                Intent in = new Intent(Rule_spendless.this, helperfunctions.AfterNewRuleReurnToClass);

                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                finish();

                startActivity(in);

            } else

                Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();

        }
    };

    Button.OnClickListener updateRule = new View.OnClickListener() {

        ArrayList<Integer> finalSelected_goals = new ArrayList<>();

        @Override
        public void onClick(View v) {

            if (objectForTowards.alert.finalselected_goals == null) {
                for (int i = 0; i < goalsdata.size(); i++) {
                    if (goalsdata.get(i).getRules_id() == ruleid)
                        finalSelected_goals.add(goalsdata.get(i).getGoals_id());
                }
            } else {
                finalSelected_goals = objectForTowards.alert.finalselected_goals;
            }

            new SqlTransactions(context).updateRuleWithMappings(getValuesFromFields(), ruleid, finalSelected_goals);
            finish();

        }
    };

    View.OnClickListener deleteRule = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.MyDialogTheme);
            dialog.setTitle("Confirm Delete");
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                    long result = new SqlTransactions(context).deleteRule(ruleid);
                    Toast.makeText(context, result != -1 ? "Rule Deleted"
                                                         : "Error deleting rule", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }).show();


        }
    };

}